/** Graph.c
 * ===========================================================
 * Name: CS220, Spring 2018
 * Modified by: Noah Diamond and Ludvig Oliver
 * Section: T3
 * Project: PEX4
 * Purpose: The implementation of a graph.
 * ===========================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <math.h>
#include "Graph.h"
#include "gameLogic.h"

/** -------------------------------------------------------------------
 * Create the memory needed to hold a graph data structure.
 * @param numberVertices the number of vertices in the graph
 * @param bytesPerNode the number of bytes used to represent a
 *                     single vertex of the graph
 * @return a pointer to a graph struct
 */
Graph * graphCreate(int numberVertices, int bytesPerNode) {
    return NULL;
}

/** -------------------------------------------------------------------
 * Delete a graph data structure
 * @param graph the graph to delete
 */
void graphDelete(Graph * graph) {
}

/** -------------------------------------------------------------------
 * Set the state of an edge in a graph
 * @param graph the graph to modify
 * @param fromVertex the beginning vertex of the edge
 * @param toVertex the ending vertex of the edge
 * @param state the state of the edge
 */
void graphSetEdge(Graph * graph, int fromVertex, int toVertex, int state) {
}

/** -------------------------------------------------------------------
 * Get the state of an edge in a graph
 * @param graph the graph
 * @param fromVertex the starting vertex of the edge
 * @param toVertex the ending vertex of the edge
 * @return the state of the edge
 */
int graphGetEdge(Graph * graph, int fromVertex, int toVertex) {
    return -1;
}