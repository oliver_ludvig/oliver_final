/* lab02.c
* ===========================================================
* Name: Troy Weingart, 4 Jan 18
* Section: n/a
* Project: Lesson 2 examples
* Purpose: Demonstrate basic c
* ===========================================================
*
 * Instructions:
 *    1) Complete TASKS Below
 */
// Includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>

#define MAXSTUDENTS 100

// define our cadet info type for later use
typedef struct {
    char name[20];
    int age;
    int squad;
    int classYear;
} MyCadetInfo;

// Prototypes for functions to be used in this file,
// remember all things in c must be declared before use
// which is why the typedef is before its use in the
// prototypes below
void printCadetInfo(MyCadetInfo cadetRecord);
int getData(MyCadetInfo datums[]);
void printData(MyCadetInfo* datums, int numCadets);
float averageAge(MyCadetInfo datums[], int numCadets);
void printAge(MyCadetInfo datums[], int numCadets, int age);
int moveSquad(MyCadetInfo datums[], int numCadets, int fromSquad, int toSquad);
bool containsCadet(MyCadetInfo datums[], int numCadets, char* name);

int main() {
    // declare an array of cadet records to hold
    // the data read from the file
    MyCadetInfo cadetRecords[MAXSTUDENTS];

    // read the cadet data into the array and store
    // the number of cadet records read from the file
    int numCadets = getData(cadetRecords);

    // TASK A - Write a function, averageAge(), that finds
    // the average age of all cadets.  The function parameters
    // are an array of MyCadetInfo typed data and the number
    // of cadets in the data set.  It will return the
    // average age of the cadets in the data set
    // 0) Create a prototype for the function
    // 1) Write the function below the end of the main() function
    // 2) Call the function and output the results

    float avgAge = averageAge(cadetRecords, numCadets);
    printf("Average age = %0.1f\n\n",avgAge);

    // TASK B - Write a function, printAge(), that prints all
    // cadets that are older than a given age.  The function
    // parameters are an array of MyCadetInfo type data, the
    // number of cadets in the data set, and the age.  The
    // function doesn't return anything
    // 0) Create a prototype for the function
    // 1) Write the function below the end of the main() function
    //    note - use existing functions to print the cadet records
    // 2) Call the function below

    printAge(cadetRecords, numCadets, 20);

    /* TASK C - Write a function, moveSquad() that moves all
     * cadets from a given squadron to another.  For example
     * if fromSquad were 10 and toSquad were 40 all cadets
     * in squad 10 would have their squadron permanently
     * changed to 40.  The function
     * parameters are an array of MyCadetInfo type data, the
     * number of cadets in the data set, the from squadron and
     * the to squadron. The function returns the number of
     * cadets that changed squadrons.
     * 0) Create a prototype for the function
     * 1) Write the function below the end of the main() function
     * 2) Call the function below and output the number of
     * cadets that changed squadrons
     */

    int numMoved = moveSquad(cadetRecords, numCadets, 33, 20);
    printf("Number moved = %d\n\n", numMoved);

    /* TASK D - CHALLENGE - Write a function, containsCadet()
     * that searches for a cadet given their full name.  This
     * function returns true or false if the cadet exists or
     * doesn't exist respectively in the array of cadet records.
     * Hint:  C doesn't have a bool by default you can look
     * for a library to support your needs or define your own
     * true and false with #defines
     * Hint:  use strcmp() to compare strings (arrays of chars)
     * cannot use ==.
     * 0) Call your function below on a cadet that is in the data
     * set and one that isn't
     */
    printf("Is Troy Weingart in the data set? %d\n",containsCadet(cadetRecords,numCadets,"Troy Weingart"));
    printf("Is Bob Sackamanis in the data set? %d\n",containsCadet(cadetRecords,numCadets,"Bob Sackamanis"));

    return 0;
}

/** ----------------------------------------------------------
 * containsCadet() determines if a cadet given a name is the data set
 * @param datums is the array of cadet records
 * @param numCadets is the number of cadets in datums
 * @param name is the name of the cadet to search for
 * @return true if found false if not
 */
bool containsCadet(MyCadetInfo datums[], int numCadets, char* name) {
    for (int i = 0; i < numCadets; i++) {
        if (strcmp(datums[i].name,name) == 0) {
            return true;
        }
    }
    return false;
}

/** ----------------------------------------------------------
 * moveSquad() moves all cadets from a given squadron to another.
 * @param datums is the array of cadet records
 * @param numCadets is the number of cadets in datums
 * @param fromSquad is the squadron to be moved from
 * @param toSquad is the squadron to be moved to
 * @return number of cadets moved
 */
int moveSquad(MyCadetInfo datums[], int numCadets, int fromSquad, int toSquad) {
    int numMove = 0;
    for (int i = 0; i < numCadets; i++) {
        if (datums[i].squad == fromSquad) {
            datums[i].squad = toSquad;
            numMove++;
        }
    }
    return numMove;
}

/** ----------------------------------------------------------
 * printAge() that prints all cadets that are older
 * than a given age.
 * @param datums is the array of cadet records
 * @param numCadets is the number of cadets in datums
 * @param age is the given age
 * @return n/a
 */
void printAge(MyCadetInfo datums[], int numCadets, int age) {
    for (int i = 0; i < numCadets; i++) {
        if (datums[i].age > age) {
            printCadetInfo(datums[i]);
        }
    }
}

/** ----------------------------------------------------------
 * averageAge() calculates the average age of
 * cadets in the given array of cadet records
 * @param datums is the array of cadet records
 * @param numCadets is the number of cadets in datums
 * @return n/a
 */
float averageAge(MyCadetInfo datums[], int numCadets) {
    int sum = 0;
    for (int i = 0; i < numCadets; i++) {
        sum += datums[i].age;
    }
    return (float) sum / numCadets;
}

/** ----------------------------------------------------------
 * printCadetInfo() is used to print a MyCadetInfo typed variable
 * to the console
 * @param cadetRecord is the cadet info struct to be printed
 * @return n/a
 */
void printCadetInfo(MyCadetInfo cadetRecord) {
    printf("Cadet name = \t%s\n", cadetRecord.name);
    printf("Cadet age = \t%d\n", cadetRecord.age);
    printf("Cadet squad = \t%d\n", cadetRecord.squad);
    printf("Cadet year = \t%d\n\n", cadetRecord.classYear);
}


/** ----------------------------------------------------------
 * getData() is used to read MyCadetInfo typed records
 * from a file
 * @param list is the array of cadet records
 * @return number of records read
 */
int getData(MyCadetInfo datums[]) {

    // Open an input file for reading
    FILE *in = fopen("../Lessons/Lsn02/lsn02Data.txt", "r");
    if (in == NULL) {
        printf("Error opening file: %s.\n", strerror(errno));
        exit(1);
    }

    char firstName[30];
    char lastName[45];
    int numRead = 0;
    while (numRead < MAXSTUDENTS && !feof(in)) {
        fscanf(in,"%s %s %d %d %d", firstName, lastName, &datums[numRead].age, &datums[numRead].squad, &datums[numRead].classYear);
        strcat(firstName," ");
        strcpy(datums[numRead].name, strcat(firstName,lastName));
        numRead++;
    }

    return numRead;
}

/** ----------------------------------------------------------
 * printData() is used to print MyCadetInfo typed records
 * from a file
 * @param datums is the array of cadet records
 * @param numCadets is the number of cadets in datums
 * @return n/a
 */
void printData(MyCadetInfo* datums, int numCadets) {
    for (int i = 0; i < numCadets; i++) {
        printCadetInfo(datums[i]);
    }
}