/** lab09.h
* ===========================================================
* Name: Troy Weingart
* Section: n/a
* Project: Lab 9 - Merge / Quick / Shell Sorts
* ===========================================================
*/
#ifndef USAFA_CS220_S18_STUDENT_LAB09_H
#define USAFA_CS220_S18_STUDENT_LAB09_H

// constant representing size of input
#define N 8

//function prototypes
/** -------------------------------------------------------------------
 * mergeSort() - Perform a mergesort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 */
void mergeSort(int array[], int lBound, int rBound);

/** -------------------------------------------------------------------
 * quickSort() - Perform a quick sort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 */
void quickSort(int array[], int lBound, int rBound);

/** -------------------------------------------------------------------
 * shellSort() - Perform a shell sort on a an array
 * @param array - the array to sort
 */
void shellSort(int array[]);

#endif //USAFA_CS220_S18_STUDENT_LAB09_H
