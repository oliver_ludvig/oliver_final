/** lab09.c
* ===========================================================
* Name: Troy Weingart
* Section: n/a
* Project: Lab 9 - Merge / Quick / Shell Sorts
* ===========================================================
* Instructions:
*    1) Complete TASKS Below
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lab09sol.h"

// Local function prototypes
void fillArray(int array[]);
void swap(int *x, int *y);
void merge(int array[], int lBound, int mid, int rBound);
int partition(int array[], int lBound, int rBound);

int main() {
//    int nums[N];

//    fillArray(nums);
    int nums1[] = {5,4,8,2,9,3,6,7};
    mergeSort(nums1, 0, N - 1);

////    fillArray(nums);
//    int nums2[] = {5,4,8,2,9,3,6,7};
//    quickSort(nums2, 0, N - 1);
//
////    fillArray(nums);
//    int nums3[] = {5,4,8,2,9,3,6,7};
//    shellSort(nums3);

    return 0;
}

/** -------------------------------------------------------------------
 * Perform a mergesort on a portion of an array, from low to high.
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 */
void mergeSort(int array[], int lBound, int rBound) {
    if (lBound < rBound) {
        int mid = (lBound + rBound) / 2;
        mergeSort(array, lBound, mid);
        mergeSort(array, mid + 1, rBound);
        merge(array, lBound, mid, rBound);
    }
}


/**
 * Given two sorted sublists array[low..mid] and array[mid+1..high],
 * merge them into a single sorted list in array[low..high]
 * @param array - the array to sort
 * @param lBound - the lowest index of the first sublist
 * @param mid - the highest index of the first sublist
 * @param rBound - the highest index of the second sublist
 */
void merge(int array[], int lBound, int mid, int rBound) {
    int tempArray[N];
    // Copy the first sublist into the tempArray
    for (int j = lBound; j <= mid; j++) {
        tempArray[j] = array[j];
    }
    // Copy the second sublist into the tempArray
    for (int j = mid + 1, k = rBound; j <= rBound; j++, k--) {
        tempArray[k] = array[j];
    }

    // Merge the two sublists
    int j = lBound;
    int k = rBound;
    int i = lBound;
    while (j <= k) {
        if (tempArray[j] < tempArray[k]) {
            array[i] = tempArray[j];
            j++;
        } else {
            array[i] = tempArray[k];
            k--;
        }
        i++;
    }
}

/**
 * partition() - partition the sublist in to two lists
 * of elements larger and smaller than the selected pivot
 * @param array - the array to sort
 * @param lBound - the left bound of the sublist
 * @param rBound - the right bound of the sublist
 */
int partition(int array[], int lBound, int rBound) {
    int pivot = array[lBound];
    int lastSmall = lBound;
    for (int i = lBound + 1; i <= rBound; i++) {
        if (array[i] < pivot) {
            lastSmall++;
            swap(&array[lastSmall], &array[i]);
        }
    }
    swap(&array[lBound], &array[lastSmall]);
    return lastSmall;  //return the division point
}

/** -------------------------------------------------------------------
 * quickSort() - Perform a quick sort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 */
void quickSort(int array[], int lBound, int rBound) {
    if (lBound < rBound) {
        int divPt = partition(array, lBound, rBound);
        quickSort(array, lBound, divPt - 1);
        quickSort(array, divPt + 1, rBound);
    }
}

/** -------------------------------------------------------------------
 * shellSort() - Perform a shell sort on a an array
 * @param array - the array to sort
 */
void shellSort(int array[]) {
    int k[] = {5,3,1};  // number/size of sets to sort
    for (int x = 0; x < 3; x++) { // sort based on each element of k[]
        for (int i = k[x]; i <= N - 1; i++) {
            int j = i - k[x];
            int val = array[i];
            // insertion sort on i'th subset
            while (j >= 0 && val < array[j]) {
                array[j + k[x]] = array[j]; // copy over until we find insertion point
                j = j - k[x];
            }
            array[j + k[x]] = val; // put value in its correct position
        }
    }
}

//swaps two integer values
void swap(int *x, int *y) {
    int temp;

    temp = *y;
    *y = *x;
    *x = temp;
}

/** ----------------------------------------------------------
 * fillArray() - fills array with random ints from 0 - N-1
 * @param array is the array of integers
 * ----------------------------------------------------------
 */
void fillArray(int array[]) {
    //set up for and then seed random number
    //generator
    static int seedDone = 0; //static variables retain their value between calls

    // modified so the seed is only done once
    if (!seedDone) {
        struct timespec time;
        clock_gettime(CLOCK_REALTIME, &time);
        srandom((unsigned) (time.tv_nsec ^ time.tv_sec));
        seedDone = 1;
    }

    // fill array with random ints from 0 to 99
    for (int i = 0; i < N; i++) {
        array[i] = (int) random() % 100;
    }
}