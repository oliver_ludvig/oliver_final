/** lab01.c
* ===========================================================
* Name: Troy Weingart, 21 Dec 17
* Section: n/a
* Project: Lab 1 - Basic C Functionality
* Purpose: Learn C basics
* ===========================================================
* Instructions:
 * Complete the tasks below without using CLion!
 * Use notepad and compilation from the Cygwin command shell (bash).
 *
 * From a Cygwin command shell type "gcc filename.c -o lab01.exe" to compile
 * and link your program.
 *
 * Use ./lab01.exe to execute your program
*/
#include <stdio.h>
#include <stdlib.h>

int main() {
    /* TASK A - Variable Basics
     * 1) prompt the user for a two integers x and y
     * 2) output the first value divided by the second value
     * 3) are you surprised by the output? (hopefully not)
     * 3) declare a float variable named quotient that is the
     *    first integer divided by the second integer value
     * 3) output quotient
     *    Note: Be sure to full precision division!  Do this by
     *    type converting an int to a float
     *    use -> (float) intVar (this is called a cast)
     */
    int intVal1 = 0;
    int intVal2 = 0;
    printf("Enter a integer value followed by an integer value (separate by space): ");
    scanf("%d %d", &intVal1,&intVal2);
    printf("%f\n", intVal1 / intVal2 );
    float quotient = intVal1 / (float) intVal2;
    printf("%f\n",quotient);


    /* TASK B - Conditional Operator
     * 1) prompt user for two values x, y
     * 2) Use the conditional operator (? :) to print x if
     *    it is larger than y otherwise print y
     */
    int x = 0;
    int y = 0;
    printf("Enter integers x,y (separate by space): ");
    scanf("%d %d",&x,&y);
    printf("%d\n",(x > y)? x : y);

    /* TASK C - Print divisors
     * 1) ask user for a lowerBound and an upperBound
     * 2) from the lower to the upper bound print out
     *    the multiples of 3 using a for loop
     * 3) accomplish part 2 again using a while loop and then a
     *    do while loop
     */
    int lowerBound = 0;
    int upperBound = 20;
    printf("Enter lower bound followed by upper bound (space separated): ");
    scanf("%d %d",&lowerBound,&upperBound);
    for (int i = lowerBound; i < upperBound; i++) {
        if (i % 3 == 0) {
            printf("%d ",i);
        }
    }
    printf("\n");


    /* TASK D - Grade Scale
     * 1) prompt the user for an integer grade value 4=A, 3=B and so on
     * 2) use a switch statement to output the corresponding letter grade
     *    Note: use default case for "F"
     */
    int gradeValue =0;
    printf("Enter grade value (0-4): ");
    scanf("%d",&gradeValue);
    switch (gradeValue) {
        case 4 :
            printf("A\n");
            break;
        case 3 :
            printf("B\n");
            break;
        case 2 :
            printf("C\n");
            break;
        case 1 :
            printf("D\n");
            break;
        default :
            printf("F\n");
    }


    /* TASK E - Guessing game in C
     * 1) generate a random integer in c, the magic number
     *          * rand() in stdlib.h may be useful
     * 2) prompt the user for an integer between 1 and 100 this will
     *    be the users guess of the magic number
     * 3) print appropriate messages if the guess is low or high relative
     *    to the magic number and allow the user to guess again
     * 4) if the user guesses the magic number output the number of guesses
     *    made -- game over
     */
    int magicNumber = rand() % 100 + 1;
    int guess = 0;
    int numGuesses = 0;
    printf("Guess a number between 1 and 100\n");
    do {
        printf("What is your guess: ");
        scanf("%d", &guess);
        numGuesses++;
        if (guess < magicNumber) {
            printf("Too low.\n");
        } else if (guess > magicNumber){
            printf("Too high.\n");
        }
    } while (guess != magicNumber);
    printf("Correct! -- %d guesses.", numGuesses);
}