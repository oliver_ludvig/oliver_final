/** lab08.c
* ===========================================================
* Name: Troy Weingart
* Section: n/a
* Project: Lab 8 - Selection / Bubble / Insertion Sorts
* ===========================================================
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lab08sol.h"


// Local function prototypes
void fillArray(int array[]);
void swap(int* x, int* y);

int main() {
    int nums[n];
//
//    fillArray(nums);
//    selSort(nums);
//
//    fillArray(nums);
//    bubSort(nums);
//
    fillArray(nums);
    insertSort(nums);


    return 0;
}

/** -------------------------------------------------------------------
 * insertSort() - Perform an insertion sort on an array
 * @param array - the array to sort
 */
void insertSort(int array[]) {
    int insertElem = 0;
    for (int i = 1; i < n; i++ ) {
        insertElem = array[i]; // the item to insert
        int j = i-1;
        while (j >= 0 && array[j] > insertElem) {   // move values up until we find the insertion
            array[j+1] = array[j];                  // point
            j--;
        }
        array[j+1] = insertElem;    // put the value in the empty slot
    }
}

/** -------------------------------------------------------------------
 * bubSort() - Perform an bubble sort on an array
 * @param array - the array to sort
 */
void bubSort(int array[]){
    //flag to indicate we can stop sorting when we
    //do not swap any values
    int isSorted = 0;

    while (!isSorted) {
        isSorted = 1; // assume the list is sorted
        for(int i = 0; i < n-1; i++) { // bubble up larger value
            if (array[i] > array[i+1]) {
                swap(&array[i],&array[i+1]);
                isSorted = 0; // we swapped so list isn't sorted
            }
        }
    }
}


/** -------------------------------------------------------------------
 * selSort() - Perform an selection sort on an array
 * @param array - the array to sort
 */
void selSort(int array[]) {
    int smallest = 0;
    int smallIndex = 0;
    // process the array from left to right
    for (int i = 0; i < n; i++) {
        // look for smallest value in the array
        smallest = array[i];
        smallIndex = i;
        for (int j = i + 1; j < n; j++) {
            if (array[j] < smallest) {
                smallest = array[j];
                smallIndex = j;            }
        }
        // once found swap it with the value in the ith
        // position
        swap(&array[i],&array[smallIndex]);
    }
}

//swaps two integer values
void swap(int* x, int* y) {
    int temp;

    temp = *y;
    *y = *x;
    *x = temp;
}

//fills an array of size n with random values
void fillArray(int array[]) {
    //set up for and then seed random number
    //generator
    struct timespec time;
    clock_gettime(CLOCK_REALTIME,&time);
    srandom((unsigned) (time.tv_nsec ^ time.tv_sec));

    // fill array with random ints from 0 to 29
    for (int i = 0; i < n; i++) {
        array[i] = (int) random() % 30;
    }
}