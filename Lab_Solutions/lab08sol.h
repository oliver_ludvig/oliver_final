/** lab08.h
* ===========================================================
* Name: Troy Weingart
* Section: n/a
* Project: Lab 8 - Selection / Bubble / Insertion Sorts
* ===========================================================
*/
#ifndef USAFA_CS220_S18_STUDENT_LAB08_H
#define USAFA_CS220_S18_STUDENT_LAB08_H

// constant representing size of input
#define n 10

/** -------------------------------------------------------------------
 * selSort() - Perform an selection sort on an array
 * @param array - the array to sort
 */
void selSort(int array[]);


/** -------------------------------------------------------------------
 * bubSort() - Perform an bubble sort on an array
 * @param array - the array to sort
 */
void bubSort(int array[]);


/** -------------------------------------------------------------------
 * insertSort() - Perform an insertion sort on an array
 * @param array - the array to sort
 */
void insertSort(int array[]);

#endif //USAFA_CS220_S18_STUDENT_LAB08_H
