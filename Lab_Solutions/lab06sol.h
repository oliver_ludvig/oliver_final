/** lab06.h
 * ===========================================================
 * Name: Troy Weingart
 * Section: n/a
 * Project: Lab 6
 * ===========================================================
 */
#ifndef USAFA_CS220_S18_STUDENT_LAB6_H
#define USAFA_CS220_S18_STUDENT_LAB6_H

#define MAXSTUDENTS 100

// define our cadet info type for later use
typedef struct {
    char name[50];
    int age;
    int squad;
    int classYear;
} MyCadetInfo;

/** ----------------------------------------------------------
 * readData() is used to read all MyCadetInfo records
 * from a binary file
 * @param cadetRecords[] is the array of stucts in memory
 * @return number of records read
 * ----------------------------------------------------------
 */
int readData(MyCadetInfo cadetRecords[]);

/** ----------------------------------------------------------
 * findIndex() is find the location of a cadet in the struct
 * in memory given the cadets name
 * @param cadetRecords[] is the array of stucts in memory
 * @param numRecs total number of records
 * @param name name of the cadet to find
 * @return location of cadet in struct or -1 on error
 * ----------------------------------------------------------
 */
int findIndex(MyCadetInfo cadetRecords[], int numRecs, char* name);


/** ----------------------------------------------------------
 * removeRec() removes a cadet in the struct given the
 * cadets name and write all data to the binary file
 * @param cadetRecords[] is the array of stucts in memory
 * @param numRecs total number of records
 * @param name name of the cadet to remove
 * @return number of records removed or -1 on error
 * ----------------------------------------------------------
 */
int removeRec(MyCadetInfo cadetRecords[], int numRecs, char* name);

/** ----------------------------------------------------------
 * readCadetBlk() is used to read a MyCadetInfo typed record
 * from a file
 * @param location is the element number of where the cadet
 * appears in the file / array of structs
 * @return number of records read
 * ----------------------------------------------------------
 */
MyCadetInfo readCadetBlk(int location);


/** ----------------------------------------------------------
 * writeDataBlk() is used to the entire cadet record arrary
 * to a binary file
 * @param cadetRecords is the array of cadet records
 * @param numCadets is the total number of cadets to write
 * @return number of records wrote
 * ----------------------------------------------------------
 */
int writeDataBlk(MyCadetInfo cadetRecords[], int numCadets);


/** ----------------------------------------------------------
 * writeCadetBlk() is used to write a single cadet records
 * to a binary file the previous contents of the file are
 * not changed
 * @param cadet is the address of the cadet structure to write
 * @param location is the offset from the from of the file to
 *                 write the record to
 * @return number of records wrote
 * ----------------------------------------------------------
 */
int writeCadetBlk(MyCadetInfo* cadet, int location);


/** ----------------------------------------------------------
 * printCadetInfo() is used to print a MyCadetInfo typed variable
 * to the console
 * @param cadetRecord is the cadet info struct to be printed
 * @return n/a
 */
void printCadetInfo(MyCadetInfo cadetRecord);


/** ----------------------------------------------------------
 * printData() is used to print MyCadetInfo typed records
 * from a file
 * @param datums is the array of cadet records
 * @param numCadets is the number of cadets in datums
 * @return n/a
 */
void printData(MyCadetInfo* datums, int numCadets);

#endif //USAFA_CS220_S18_STUDENT_LAB6_H
