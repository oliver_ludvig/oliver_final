/** lab15.c
* ================================================================
* Name: first last
* Section: xxx
* Project: Lab15 - Dynamic Memory Allocation
* =================================================================
*
* Instructions:  In this lab you will practice allocating memory
* dynamically, using the c library functions malloc() and
* realloc().  Specifically you will allocate an array using malloc()
* and then dynamically grow this array using realloc() as it is filled
* with random integers.
*
* Complete the tasks below.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define INIT_SIZE 1000

//local function prototypes
void seedGenerator();
int fillArray(int *array, int arraySize, int dataSetSize);
void printArray(int *array, int arraySize);

int main() {

    /** TASK 0 - finish the seedGenerator() function.
     * This function seeds the random number
     * generator (see lab 9's fill array function).  This function
     * should only allow the srand() to be called once. Call the
     * function below.
     */
    seedGenerator();

    /** TASK 1 - Allocate an array of INIT_SIZE integers
     * using the malloc() function call.
     */
    int *array = malloc(INIT_SIZE * sizeof(int));

    /** TASK 2 - declare and initialize an int variable to
     * hold the size of the data set of random integers.
     * The size should vary between 1 and 100000.
     */
    int size = ((int) random() % 100000);

    /** TASK 3 - complete the fillArray() function implementation
     * and call it.
     * part 1 - implement fillArray() using realloc() (header below)
     *    Note: double the array in size when it is reallocated
     * part 2 - call the function and print out a message
     * that includes the initial size of the array, the
     * randomly generated data set size, and the number
     * of times doubled.
     */
    fillArray(array, INIT_SIZE, size);

    return 0;
}

/** ----------------------------------------------------------
 * fillArray() - fills array with random ints its size is
 * specfied by dataSetSize
 * @param array is a ptr to an array of integers
 * @param arraySize is the size of the array of integers
 * @param dataSetSize is the final size of the array of integers
 * which can be smaller, equal to, or larger than the array's initial
 * size
 * @return the number of times the initial array was doubled in size
 * to support the larger data set
 * ----------------------------------------------------------
 */
int fillArray(int *array, int arraySize, int dataSetSize) {
    int count = 0;
    int multiply = 2;
    printf("%d\n", dataSetSize);
    printf("initial size of array: %d\n", arraySize);
    seedGenerator();
    for (int i = 0; i < dataSetSize; i++) {
        if (i >= arraySize) {
            array = realloc(array, sizeof(array)* multiply);
            array[i] = (int) random() % 2500;
            arraySize = arraySize * multiply;
            multiply = multiply * 2;
            count++;
        }
        else
            array[i] = (int) random() % 2500;
    }
    printArray(array, arraySize);
    return count;
}

/** ----------------------------------------------------------
 * seedGenerator() - seeds the random number generator only once.
 * ----------------------------------------------------------
 */
void seedGenerator() {

    //set up for and then seed random number
    //generator
    static int seedDone = 0; //static variables retain their value between calls

    // modified so the seed is only done once
    if (!seedDone) {
        struct timespec time;
        clock_gettime(CLOCK_REALTIME, &time);
        srandom((unsigned) (time.tv_nsec ^ time.tv_sec));
        seedDone = 1;
    }
}

void printArray(int *array, int arraySize) {
    for (int i = 0; i < arraySize; i++) {
        printf("%d ", array[i]);
    }
}