//
// Created by C20Ludvig.Oliver on 2/16/2018.
//

#include <stdio.h>
#include <stdlib.h>
#include "listAsLinkedList.h"

int main() {

    return 0;
}

LinkedList* createLinkedList() {
    LinkedList* newList = malloc(sizeof(LinkedList));
    newList -> head = NULL;
    newList -> tail = NULL;
    newList -> numberOfElements = 0;
    return newList;
}

void deleteLinkedList(LinkedList* list) {
    Node* tempPtr = list->head;
    Node* freePtr = list->head;
    while (tempPtr != NULL){
        tempPtr = tempPtr -> next;
        free(freePtr);
        freePtr = tempPtr;
    }
    list -> head = NULL;
    list -> tail = NULL;
    list -> numberOfElements = 0;
}

void appendElementLinkedList(LinkedList* list, int element) {
    Node* newNode = malloc(sizeof(Node));  // Creates memory space of node
    newNode->data = element;       // made data into desired element
    newNode->next = NULL;       // establishes next as null
    list->tail->next = newNode;     // creates the list.tail.next to become the newNode
    list->tail = newNode;       // the tail is now the newNode
    list->numberOfElements += 1;  // adds 1 to the element
}

int lengthOfLinkedList(LinkedList* list) {
    return list->numberOfElements;
}

void printLinkedList(LinkedList* list) {
    Node* tempPtr = list->head;
    while (tempPtr != NULL) {
        printf("%d", tempPtr -> data);
        tempPtr = tempPtr -> next;
    }
}

int getElementLinkedList(LinkedList* list, int position) {
    int place = 0;
    Node* tempPtr = list->head;
    while (tempPtr != NULL){
        place += 1;
        if (place == position) {
            return tempPtr -> data;
        }
        tempPtr = tempPtr -> next;
    }
    return -1;
}

void changeElementLinkedList(LinkedList* list, int position, int newElement) {
    int place = 0;
    Node* tempPtr = list -> head;
    while (tempPtr != NULL){
        place += 1;
        if (place == position) {
            tempPtr -> data = newElement;
            return;
        }
        tempPtr = tempPtr -> next;
    }
}

void deleteElementLinkedList(LinkedList* list, int position) {
    if (position > list->numberOfElements) {
        return;
    }
    else if (position == 1) {
        Node* tempPtr = list->head;
        list -> head = tempPtr -> next;
        free(tempPtr);
        list -> numberOfElements --;
    }
    else if (position == list->numberOfElements) {
        Node* tempPtr = list -> head;
        int i = 0;
        while (i < position - 1) {
            tempPtr = tempPtr -> next;
            i++;
        }
        list -> tail = tempPtr;
        free(tempPtr -> next);
        tempPtr -> next = NULL;
        list -> numberOfElements --;
    }
    else {
        int i = 0;
        Node *tempPtr = list -> head;
        while (i < position - 1) {
            i++;
            tempPtr = tempPtr -> next;
        }
        tempPtr -> next = tempPtr -> next -> next;
        free(tempPtr -> next);
        list -> numberOfElements --;
    }
}
void insertElementLinkedList(LinkedList* list, int position, int element){
    if (position > list -> numberOfElements) {
        return;
    }
    // Create New Node
    Node* newNode = malloc(sizeof(Node));
    newNode -> data = element;
    newNode -> next = NULL;
    // insert node based on cases
    if (list -> numberOfElements == 0) {
        list -> head = newNode;
        list -> tail = newNode;
        list -> numberOfElements ++;
    }
    else if (position == 0) {
        newNode -> next = list -> head;
        list -> head = newNode;
        list -> numberOfElements ++;
    }
    else if (position == list -> numberOfElements + 1) {
        appendElementLinkedList(list, element);
    }
    else {
        Node* tempPtr = list -> head;
        int i = 0;
        while (i < position - 1) {
            i++;
            tempPtr = tempPtr -> next;  // tempPtr is pointing to the new node previously
        }
        newNode -> next = tempPtr -> next;
        tempPtr -> next = newNode;
        list -> numberOfElements ++;
    }
}

int findElementLinkedList(LinkedList* list, int element){
    Node* tempPtr = list -> head;
    int size = list -> numberOfElements;
    int i = 0;
    while (i < size) {
        i++;
        if (tempPtr -> data == element) {
            return i;
        }
        tempPtr = tempPtr -> next;
    }
    return -1;
}
void insertSortLinkedList(LinkedList* list){

}