/** lab08.c
* ===========================================================
* Name: Troy Weingart
* Section: n/a
* Project: Lab 8 - Selection / Bubble / Insertion Sorts
* ===========================================================
* Instructions:
*    1) Complete TASKS Below
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lab08.h"

// Local function prototypes follow
void fillArray(int array[]);
void swap(int* x, int* y);

int main() {
    int nums[n];
    
    // fill the array with random numbers
    fillArray(nums);
    
    /* TASK 0 - implement the function, selSort() below
     * 0) Call the function and verify that it works
     */
    selSort(nums);

    /* TASK 1 - implement the function, bubSort() below
     * 0) Refill the array to get a new random set of 
     * numbers.
     * 1) Call the function and verify that it works
     */
    fillArray(nums);
    bubSort(nums);

    /* TASK 2 - implement the function, insertSort() below
     * 0) Refill the array to get a new random set of 
     * numbers.
     * 1) Call the function and verify that it works
     */
    fillArray(nums);
    insertSort(nums);

    return 0;
}

// Insertion Sort
void insertSort(int array[]) {

    for (int i = 0; i < n; i++) {
        for (int x = 0; x < n; x++) {
            if (array[i] < array[x])
                swap(&array[i], &array[x]);
        }
    }
    printf("\nInsert Sort\n");
    for (int i = 0; i < n; i++) {
        printf("%d ", array[i]);
    }
}

// Bubble Sort
void bubSort(int array[]){

}

// Selection Sort
void selSort(int array[]) {

    for (int i = 0; i < n; i++) {
        for (int x = 0; x < n; x++) {
            if (array[i] < array[x])
                swap(&array[i], &array[x]);
        }
    }

    printf("\nSelection Sort\n");
    for (int i = 0; i < n; i++) {
        printf("%d ", array[i]);
    }
}

//swaps two integer values
void swap(int* x, int* y) {
    int temp;

    temp = *y;
    *y = *x;
    *x = temp;
}

//fills an array of size n with random values
void fillArray(int array[]) {
    //set up for and then seed random number
    //generator
    struct timespec time;
    clock_gettime(CLOCK_REALTIME,&time);
    srandom((unsigned) (time.tv_nsec ^ time.tv_sec));

    // fill array with random ints from 0 to 29
    for (int i = 0; i < n; i++) {
        array[i] = (int) random() % 30;
    }
}
