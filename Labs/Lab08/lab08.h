//
// Created by Troy.Weingart on 1/21/2018.
//
#ifndef USAFA_CS220_S18_STUDENT_LAB08_H
#define USAFA_CS220_S18_STUDENT_LAB08_H

// constant representing size of input
#define n 10

//function prototypes
void selSort(int array[]);
void bubSort(int array[]);
void insertSort(int array[]);

#endif //USAFA_CS220_S18_STUDENT_LAB08_H
