/** lab09.c
* ===========================================================
* Name: First Last
* Section: xxx
* Project: Lab 9 - Merge / Quick / Shell Sorts
* ===========================================================
* Instructions:
*    1) Complete TASKS Below
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lab09.h"

// Local function prototypes
void fillArray(int array[]);
void swap(int* x, int* y);
void merge(int array[], int lBound, int mid, int rBound);
int partition(int array[], int lBound, int rBound);

int main() {
    int nums[N];

     /* TASK 0 - implement the function, mergeSort() below
      *  Help - the merge prototype is given to you above
      */
    fillArray(nums);
    mergeSort(nums,0,N-1);

    /* TASK 1 - implement the function, quickSort() below
     * Help - the partition prototype is given to you above
     */
    fillArray(nums);
    quickSort(nums,0,N-1);

    /* TASK 2 - implement the function, shellSort() below
     * using diminishing step values of 5,3,1
     */
    fillArray(nums);
    shellSort(nums);

    return 0;
}

/**
 * merge() - Given two sorted sublists array[lBound..mid] and array[mid+1..rBound],
 * merge them into a single sorted list in array[lBound..rBound]
 * @param array - the array to sort
 * @param lBound - the lowest index of the first sublist
 * @param mid - the highest index of the first sublist
 * @param rBound - the highest index of the second sublist
 */
void merge(int array[], int lBound, int mid, int rBound) {

}

/** -------------------------------------------------------------------
 * mergeSort() - Perform a mergesort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 */
void mergeSort(int array[], int lBound, int rBound) {

}

/**
 * partition() - partition the sublist in to two lists
 * of elements larger and smaller than the selected pivot
 * @param array - the array to sort
 * @param lBound - the left bound of the sublist
 * @param rBound - the right bound of the sublist
 */
int partition(int array[], int lBound, int rBound) {

}


/** -------------------------------------------------------------------
 * quickSort() - Perform a quick sort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 */
void quickSort(int array[], int lBound, int rBound) {

}

/** -------------------------------------------------------------------
 * shellSort() - Perform a shell sort on a an array
 * @param array - the array to sort
 */
void shellSort(int array[]) {

}

//swaps two integer values
void swap(int* x, int* y) {
    int temp;

    temp = *y;
    *y = *x;
    *x = temp;
}

//fills an array of size n with random values
void fillArray(int array[]) {
    //set up for and then seed random number
    //generator
    static int seedDone = 0; //static variables retain their value between calls

    // modified so the seed is only done once
    if (!seedDone) {
        struct timespec time;
        clock_gettime(CLOCK_REALTIME, &time);
        srandom((unsigned) (time.tv_nsec ^ time.tv_sec));
        seedDone = 1;
    }

    // fill array with random ints from 0 to 99
    for (int i = 0; i < N; i++) {
        array[i] = (int) random() % 100;
    }
}