/** lab10.c
* ===========================================================
* Name: First Last
* Section: xxx
* Project: Lab10
* ===========================================================
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// for now lets worry only about square matrices so M == N
#define M 5
#define N 5

// Local function prototypes
void fillMatrix(int matrix[M][N]);
void printMatrix(int matrix[M][N]);
void addMatrix(int matrixA[M][N], int matrixB[M][N], int matrixC[M][N]);
void multMatrix(int matrixA[M][N], int matrixB[M][N], int matrixC[M][N]);

int main() {

    // declare some matrices to work with
    int matrixA[M][N];
    int matrixB[M][N];
    int matrixC[M][N];

    /* TASK 0 - write printMatrix() - print matrix
     * takes as input an MxN matrix and prints it
     * as you would expect
     * 0) Function prototype is given above
     * 1) Uncomment the code below and
     * verify that your print matrix function works
     * Hint:  to get column alignments
     * use %5d as your format specifier
     * in your printf()
     */
    fillMatrix(matrixA);
    printMatrix(matrixA);
    fillMatrix(matrixB);
    printMatrix(matrixB);

    /* TASK 1 - write addMatrix() - add matrix
     * takes as input two MxN matrices and adds
     * them storing the result in a 3rd matrix
     * 0) Function prototype is given above
     * 1) Uncomment the code below and
     * verify that your add matrix function works
     */
    addMatrix(matrixA, matrixB, matrixC);
    printMatrix(matrixC);

    /* TASK 2 - write multMatrix() - multiply matrix
     * takes as input two MxN matrices and does a linear
     * algebra multiply (don't just multiply corresponding
     * elements in the matrices) and stores the result in
     * a 3rd matrix
     * for more -> https://www.mathsisfun.com/algebra/matrix-multiplying.html
     * 0) Function prototype is given above
     * 1) Uncomment the code below and
     * verify that your multiply matrix function works
     */
//    multMatrix(matrixA, matrixB, matrixC);
//    printMatrix(matrixC);

    return 0;
}

void multMatrix(int matrixA[M][N], int matrixB[M][N], int matrixC[M][N]) {

    for (int i = 0; i < M; i++) {
        for (int x = 0; x < N; x++) {
            matrixC[i][x] = (matrixA[i][x] * matrixB[x][i]);
        }
    }

}

void addMatrix(int matrixA[M][N], int matrixB[M][N], int matrixC[M][N]) {

    for (int i = 0; i < M; i++) {
        for (int x = 0; x < N; x++) {
            matrixC[i][x] = matrixA[i][x] + matrixB[i][x];
        }
    }

}

void printMatrix(int matrix[M][N]) {
    for (int r = 0; r < M; r++) {
        for (int c = 0; c < N; c++) {
            printf("%5d", matrix[r][c]);
        }
        printf("\n");
    }
    printf("\n");
}

//fills an matrix of size MxN with random values
void fillMatrix(int matrix[M][N]) {
    //set up for and then seed random number generator
    static int seedDone = 0; //static variables retain their value between calls

    // if random number generator has been seeded already don't do it again
    if (!seedDone) {
        struct timespec time;
        clock_gettime(CLOCK_REALTIME, &time);
        srandom((unsigned) (time.tv_nsec ^ time.tv_sec));
        seedDone = 1;
    }

    // fill matrix with random ints
    for (int r = 0;r < M; r++) {
        for (int c = 0; c < N; c++) {
            matrix[r][c] = random() % (M+N);
        }
    }
}
