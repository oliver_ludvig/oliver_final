/** lab07.c
* ===========================================================
* Name: fist last
* Section: xxx
* Project: Lab 7 - Insert & Binary Search
* ===========================================================
*
* Introduction: Over the next few lessons we will be creating a
* simple cadet database program...
*
* Instructions:
*    1) Complete TASKS Below
*/
#include "lab07.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

int main() {
    MyCadetInfo cadetRecs[MAXSTUDENTS];

    // Read in cadet info
    int numCadets = readData(cadetRecs);
    printf("nunCadets = %d\n", numCadets);

    /* TASK 0 - complete a function, nameCmp(), that compares two
     * proper names (first and last only)
     * assumptions: No spaces in first or last names
     * Note: This function takes two names and returns a negative number
     * if the first comes before the second, a 0 if they are equal,
     * and a positive number if the first comes after the second
     * Hint: use strcasecmp() - case insensitive version of strncmp()
     * Hint: starter code is provided for you as well as call below
     * 0) Complete nameCmp() below
     */
    char name1[] = {"Troy Weingart"};
    char name2[] = {"Bill Weingart"};
    printf("retVal = %d\n",nameCmp(name1,name2));

    /* TASK 1 - write a function, binSearch() that finds the index of
     * a cadet given the cadets name using a binary search
     * 0) The prototype has been completed for you, complete this
     * function below
     * 1) Call your function below and verify that it works
     * hint: the initial call will use 0 as the left bound and
     * numCadets - 1 as the right
     * hint: recursive function is much much cleaner and shorter!
     * 1) What is the Big-O of this function?
     *          Answer:
     */

    return 0;
}

/** ----------------------------------------------------------
 * binSearch() searches database for a given cadet using a
 * binary search
 * @param cadetRecords[] is the array of stucts in memory
 * @param lBound is the left index of the part of the
 * array to search
 * @param rBound is the right index of the part of the
 * array to search
 * @param name is the name of the cadet to find
 * @return returns -1 on not found or the index of the cadet
 * ----------------------------------------------------------
 */
int binSearch(MyCadetInfo cadetRecords[], int lBound, int rBound, char* name) {
    return -1;
}


/** ----------------------------------------------------------
 * nameCmp() compares two names
 * @param nume1 name of first
 * @param name2 name of second
 * @return returns <0 if first comes before 2nd 0 if equal or
 * >0 if 1st comes after 2nd
 * ----------------------------------------------------------
 */
int nameCmp(char name1[], char name2[]){
    //strsep modifies the string it's give so make a
    //copy of the input parameters
    //strdup allocates memory for the copy of the
    //string...cool
    char* tempName1 = strdup(name1);
    char* tempName2 = strdup(name2);

    // strsep separates strings on a delimeter
    // and modifies the address of the string in
    // memory which is why we duplicate the strings
    // below

    char* firstName1 = strdup(strsep(&tempName1," "));
    char* firstName2 = strdup(strsep(&tempName2," "));


    // use strcasecmp to check last names and if
    // needed first names...after the strsep() call the
    // lastname in each tempName is now pointed to by
    // tempName as strsep changes where tempName points

    int val = strcasecmp(tempName1,tempName2);


    // if the last names are different return
    // val as we do not need to compare first
    // names
    // SUB TASK - write if statement to return val
    // in case where the lastnames are not the
    // same

    if (val != 0)
        return -1;

    // last names are the same so compare first names
    // SUB TASK - call strcasecmp() and return result

    val = strcasecmp(firstName1, firstName2);



    return val;
}
/** ----------------------------------------------------------
 * removeRec() removes a cadet in the struct given the
 * cadets name and write all data to the binary file
 * @param cadetRecords[] is the array of stucts in memory
 * @param numRecs total number of records
 * @param name name of the cadet to remove
 * @return number of records removed or -1 on error
 * ----------------------------------------------------------
 */
int removeRec(MyCadetInfo cadetRecords[], int numRecs, char* name) {
    int index = findIndex(cadetRecords, numRecs, name);

    // error name not found
    if (index == -1) {
        return -1;
    }

    // copy down the records to write over the removed record
    // as the last entry isn't the one to be removed
    if (index != numRecs-1) {
        for (int i = index; i < numRecs - 1; i++) {
            cadetRecords[i] = cadetRecords[i + 1];
        }
    }
    //writedb & return
    writeDataBlk(cadetRecords,numRecs-1);
    return 1;
}


/** ----------------------------------------------------------
 * findIndex() is find the location of a cadet in the struct
 * in memory given the cadets name
 * @param cadetRecords[] is the array of stucts in memory
 * @param numRecs total number of records
 * @param name name of the cadet to find
 * @return location of cadet in struct or -1 on error
 * ----------------------------------------------------------
 */
int findIndex(MyCadetInfo cadetRecords[], int numRecs, char* name) {
    for (int i = 0; i < numRecs; i++) {
        if (strncmp(cadetRecords[i].name,name,50) == 0) {
            return i;
        }
    }
    return -1;
}


/** ----------------------------------------------------------
 * readData() is used to read all MyCadetInfo records
 * from a binary file
 * @param cadetRecords[] is the array of stucts in memory
 * @return number of records read
 * ----------------------------------------------------------
 */
int readData(MyCadetInfo cadetRecords[]) {
    // Open an input file for reading
    FILE* in = fopen("../Labs/Lab07/lab07Data.dat", "r");
    if (in == NULL) {
        printf("Error opening data file: %s.\n", strerror(errno));
        exit(1);
    }
    size_t numRead = 0;
    numRead = fread(cadetRecords, sizeof(MyCadetInfo), MAXSTUDENTS,in);
    fclose(in);

    return numRead;
}

/** ----------------------------------------------------------
 * readCadetBlk() is used to read a MyCadetInfo typed record
 * from a file
 * @param location is the element number of where the cadet
 * appears in the file / array of structs
 * @return number of records read
 * ----------------------------------------------------------
 */
MyCadetInfo readCadetBlk(int location) {

    // Open an data file for reading
    FILE *in = fopen("../Labs/Lab07/lab07Data.dat", "r");
    if (in == NULL) {
        printf("Error opening data file: %s.\n", strerror(errno));
        exit(1);
    }

    MyCadetInfo tempCadet;

    fseek(in, sizeof(MyCadetInfo) * (long) location, SEEK_SET);
    fread(&tempCadet, sizeof(MyCadetInfo), 1, in);
    fclose(in);
    return tempCadet;
}

/** ----------------------------------------------------------
 * writeDataBlk() is used to the entire cadet record array
 * to a binary file
 * @param cadetRecords is the array of cadet records
 * @param numCadets is the total number of cadets to write
 * @return number of records wrote
 * ----------------------------------------------------------
 */
int writeDataBlk(MyCadetInfo cadetRecords[], int numCadets) {

    // Open an output file for writing
    FILE *out = fopen("../Labs/Lab07/lab07Data.dat", "w");
    if (out == NULL) {
        printf("Error creating data file: %s.\n", strerror(errno));
        exit(1);
    }

    size_t retVal = fwrite(cadetRecords, sizeof(MyCadetInfo), numCadets, out);
    fclose(out);
    return retVal;

}

/** ----------------------------------------------------------
 * writeCadetBlk() is used to write a single cadet records
 * to a binary file the previous contents of the file are
 * not changed
 * @param cadet is the address of the cadet structure to write
 * @param location is the offset from the from of the file to
 *                 write the record to
 * @return number of records wrote
 * ----------------------------------------------------------
 */
int writeCadetBlk(MyCadetInfo *cadet, int location) {

    // Open an output file for writing
    FILE *out = fopen("../Labs/Lab07/lab07Data.dat", "r+");
    if (out == NULL) {
        printf("Error creating data file: %s.\n", strerror(errno));
        exit(1);
    }
    fseek(out, sizeof(MyCadetInfo) * (long) location, SEEK_SET);
    int retVal = fwrite(cadet, sizeof(MyCadetInfo), 1, out);
    fclose(out);
    return retVal;
}

/** ----------------------------------------------------------
 * printCadetInfo() is used to print a MyCadetInfo typed variable
 * to the console
 * @param cadetRecord is the cadet info struct to be printed
 * @return n/a
 */
void printCadetInfo(MyCadetInfo cadetRecord) {
    printf("Cadet name = \t%s\n", cadetRecord.name);
    printf("Cadet age = \t%d\n", cadetRecord.age);
    printf("Cadet squad = \t%d\n", cadetRecord.squad);
    printf("Cadet year = \t%d\n\n", cadetRecord.classYear);
}

/** ----------------------------------------------------------
 * printData() is used to print MyCadetInfo typed records
 * from a file
 * @param datums is the array of cadet records
 * @param numCadets is the number of cadets in datums
 * @return n/a
 */
void printData(MyCadetInfo *datums, int numCadets) {
    for (int i = 0; i < numCadets; i++) {
        printCadetInfo(datums[i]);
    }
}