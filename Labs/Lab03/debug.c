/** debug.c
* ===========================================================
* Name: Troy Weingart, 8 Jan 18
* Section: n/a
* Project: Debugging exercise
* Purpose: Practice debugging in C
* ===========================================================
*
 * Instructions:
 *  0) Find and fix all syntax and logic errors in debug.c &
 *  debug.h  Note:  You should fix compiler warnings and make
 *  use of the debugger.  You may also find that adding printf
 *  statements in key positions helpful
 *  1) You should find 10 issues, list them here
 *  error 1: void in debug.h (prototype) instead of int for sumEvents
 *  error 2: required #include <stdio.h> to utilize printf
 *  error 3: required #include "debug.h" to include prototype for functions
 *  error 4: required "return sum" to receive a output for sumEvens function
 *  error 5: shift int x to the bottom of main underneath char y[] to run the program
 *  error 6: Changed i-- to i++ to go up to the desired number
 *  error 7: Include const on both the WhatDoIDo function in main and header
 *  error 8:
 *  error 9:
 *  error 10
 */

#include <stdio.h>
#include "debug.h"


int main() {
    printf("Int sum = %d\n", sumEvens(10));

    int a[] = {1, 10, 3, 7, 5, 0, 2, 1, 4, 102, -99};
    char y[] = {"WitnessThePowerOfC"};
    int x = whatDoIDo(a, y);


    return 0;
}

// Sums the even numbers up to num (after you fix it that is)
int sumEvens(int num) {
    int sum = 0;
    int rem;
    int i;
    for (i = 0; i < num + 1; i++)
    {
        rem = i % 2;
        if (rem == 0) {
            sum += i;
        }
    }
    return sum;
}

// Use the debugger to follow the execution of this function
// and determine what it does
int whatDoIDo(const int* x, char* y) {
    int z=0, i = 0;
    while (*(x+i) != -99) {
        if (!(*(x+i) % 2)) {
            *(y+i) = '*';
            z++;
        }
        i++;
    }
    return z;
}