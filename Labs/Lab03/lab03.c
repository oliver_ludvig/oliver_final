/* lab03.c
* ===========================================================
* Name: Ludvig Oliver
* Section: T3/T4
* Project: Lesson 3 Lab
* Purpose: Practice with header files & recursion
* ===========================================================
*
 * Instructions:
 *    1) Complete TASKS Below
 */
// Includes
#include <stdio.h>
#include "lab03.h"

int main() {
    /* TASK A - We have triangle made of blocks. The topmost row
     * has 1 block, the next row down has 2 blocks, the next row has
     * 3 blocks, and so on. Compute recursively (no loops or multiplication)
     * the total number of blocks in such a triangle with the given number of rows.
     * 0) A header file with the function prototype is included for
     *    you.
     * 1) Identify the base case and recursive case
     * 2) Implement your function below main()
     * 3) The setup and function call are included below
     */
    int rows = 0;
    printf("Enter a number of rows: ");
    scanf("%d", &rows);
    printf("The number of blocks in a triangle with %d rows, is %d\n", rows, triBlocksRec(rows));

    /* TASK B - Given a non-negative int n, write a function that returns
     * the sum of its digits recursively (no loops). Note that mod (%) by 10
     * yields the rightmost digit (126 % 10 is 6), while divide (/) by 10
     * removes the rightmost digit (126 / 10 is 12).
     * 0) A header file with the function prototype is included for
     *    you.
     * 1) Identify the base case and recursive case
     * 2) Implement your function below main()
     * 3) The setup and function call are included below
     *
     */
    int num = 0;
    printf("Enter an integer to sum: ");
    scanf("%d", &num);
    printf("This sum of the digits of %d is %d\n", num, sumDigitsRec(num));

    /* TASK C - Write a function that when given an array of ints,
     * the index of the last element, and a given number will determine
     * recursively if the array contains the given number.
     * 0) A header file with the function prototype is included for
     *    you.
     * 1) Identify the base case and recursive case
     * 2) Implement your function below main()
     * 3) The setup and function call are included below
     *
     */
    // Declare an array of ints and initialize a variable
    // for the number to find in the array
    int nums[] = {3, 1, 7, 33, 2, 1, 5, 1};
    int numToFind = 0;

    printf("Enter a number to find: ");
    scanf("%d", &numToFind);

    // Compute the size of the array
    int sizeOfNums = sizeof(nums) / sizeof(nums[0]);
    printf("Is there a %d in this array: %d\n", numToFind, arrayContainsRec(nums, sizeOfNums - 1, numToFind));

    return 0;
}
/* ----------------------------------------------------------
 * triBlocksRec() - computes the number of blocks in a triangle
 * stack
 * @param rows is the number of rows in the stack of blocks
 * @return number of rows
 * ----------------------------------------------------------
 */
int triBlocksRec(int rows) {
    if (rows <= 0)
        return 0;
    else
        return rows + triBlocksRec(rows - 1);
}

/* ----------------------------------------------------------
 * sumDigitsRec() adds the individual digits of an integer
 * @param num the integer to sum
 * @return the sum
 * ----------------------------------------------------------
 */
int sumDigitsRec(int num) {
    if (num <= 0)
        return 0;
    else
        return num % 10 + sumDigitsRec( num / 10);
}

/* ----------------------------------------------------------
 * arrayContainsRec() determines if a given number is
 * contained in an arry
 * @param nums the array of integers
 * @param index the start index of operation (typically the end)
 * @param value the number you are looking for
 * @return true if contains value
 * ----------------------------------------------------------
 */
bool arrayContainsRec(int nums[], int index, int value) {
    if (nums[index] == '\0')
        return false;
    if (nums[index] == value)
        return true;
    else
        arrayContainsRec(nums, index - 1, value);
}


