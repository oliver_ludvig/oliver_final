/** debug.h
* ===========================================================
* Name: Troy Weingart, 8 Jan 18
* Section: n/a
* Project: Debugging exercise
* Purpose: Practice debugging in C
* ===========================================================
*/
#ifndef USAFA_CS220_S18_STUDENT_DEBUG_H
#define USAFA_CS220_S18_STUDENT_DEBUG_H

int sumEvens(int num);
int whatDoIDo(const int* x, char* y);

#endif //USAFA_CS220_S18_STUDENT_DEBUG_H
