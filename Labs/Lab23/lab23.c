/** lab23.c
* ================================================================
* Name: Ludvig Oliver
* Section: T3/T4
* Project: Header for BST library
* =================================================================
*/
#include <stdio.h>
#include "bstAsLinkedDS.h"

/* Instructions - Complete the following tasks
 *
 * Task0 - complete insertBST as defined in the
 * header file.  Test your implementation.  Note:
 * Use a recursive helper function to implement this
 * function.
 *
 * Task1 - complete the 3 traversals as defined
 * in the header file.  Test your implementation.
 *
 *
 */
int main() {
    BSTnode* root = NULL;
    root = insertBST(root,5);
    root = insertBST(root,10);
    root = insertBST(root,1);
    root = insertBST(root,7);
    root = insertBST(root,3);
    root = insertBST(root,6);
    printInorderBST(root);printf("\n");
    printPreorderBST(root);printf("\n");
    printPostorderBST(root);printf("\n");
}

BSTnode* insertBST(BSTnode* node, int item) {

    BSTnode *newNode = malloc(sizeof(BSTnode));
    newNode->data = item;
    newNode->left = NULL;
    newNode->right = NULL;

    if (node == NULL) {
        return newNode;
    } else {
        return insertBSTree(node, newNode);
    }

}

BSTnode* insertBSTree(BSTnode* root, BSTnode* newNode) {

    if (root == NULL) {
        return newNode;
    }
    if (newNode->data < root->data) {
        root->left = insertBSTree(root->left, newNode);
    } else {
        root->right = insertBSTree(root->right, newNode);
    }

    return root;

}

void printInorderBST(BSTnode* nodePtr) {

    if (nodePtr == NULL) {
        return;
    }

    printInorderBST(nodePtr->left);

    printf("%d", nodePtr->data);

    printInorderBST(nodePtr->right);

}

void printPreorderBST(BSTnode* nodePtr) {

    if (nodePtr == NULL) {
        return;
    }

    printf("%d", nodePtr->data);

    printPreorderBST(nodePtr->left);

    printPreorderBST(nodePtr->right);

}

void printPostorderBST(BSTnode* nodePtr) {

    if (nodePtr == NULL) {
        return;
    }

    printPostorderBST(nodePtr->left);

    printPostorderBST(nodePtr->right);

    printf("%d", nodePtr->data);

}
