/** bstAsLinkedDT.h
* ================================================================
* Name: first last
* Section: xxx
* Project: Header for BST library
* =================================================================
*/

#ifndef USAFA_CS220_S18_STUDENT_BSTASLINKEDDS_H
#define USAFA_CS220_S18_STUDENT_BSTASLINKEDDS_H

#include <stdio.h>
#include <stdbool.h>

typedef struct _bstNode {
    int data;
    struct _bstNode* left;
    struct _bstNode* right;
} BSTnode;

/** ----------------------------------------------------------
 * searchBST() searches for an item in a BST
 * @param node - a pointer to the root of the BST to search
 * @param item - the data item to search for
 * @return returns a pointer to the found node NULL if not found
 * ----------------------------------------------------------
 */
BSTnode* searchBST(BSTnode* node, int item);

/** ----------------------------------------------------------
 * containsBST() searches for an item in a BST
 * @param node - a pointer to the root of the BST to search
 * @param item - the data item to search for
 * @return returns true if found or false if not
 * ----------------------------------------------------------
 */
bool containsBST(BSTnode* node, int item);


/** ----------------------------------------------------------
 * sizeBST() counts the nodes in a BST
 * @param node - a pointer to the root of the BST
 * @return returns number of nodes
 * ----------------------------------------------------------
 */
int sizeBST(BSTnode* node);


/** ----------------------------------------------------------
 * heightBST() calcuates the height of the BST
 * @param node - a pointer to the root of the BST
 * @return returns height
 * ----------------------------------------------------------
 */
int heightBST(BSTnode* node);


/** ----------------------------------------------------------
 * heightBST() frees memory in a BST
 * @param node - a pointer to the root of the BST
 * ----------------------------------------------------------
 */
void destroyBST(BSTnode* node);


/** ----------------------------------------------------------
 * deleteNodeBST() deletes a single node from a BST
 * @param node - a pointer to the root of the BST
 * @param item - the item to find and delete
 * @return pointer to remaining tree after item was
 * deleted.
 * ----------------------------------------------------------
 */
BSTnode* deleteNodeBST(BSTnode* node, int item);

/** ----------------------------------------------------------
 * insertBST() inserts an item into a BST
 * @param node - a pointer to the root of the BST to instert
 * the item in.
 * @param item - the data item to insert into the BST
 * @return returns a pointer to the root of the BST
 * ----------------------------------------------------------
 */
BSTnode* insertBST(BSTnode* node, int item);


/** ----------------------------------------------------------
 * printInorderBST() traverses and prints the BST
 * inorder.
 * @param node - a pointer to the root of the BST
 * ----------------------------------------------------------
 */
void printInorderBST(BSTnode* nodePtr);


/** ----------------------------------------------------------
 * printPreorderBST() traverses and prints the BST
 * preorder.
 * @param node - a pointer to the root of the BST to
 * ----------------------------------------------------------
 */
void printPreorderBST(BSTnode* nodePtr);


/** ----------------------------------------------------------
 * printPostorderBST() traverses and prints the BST
 * postorder.
 * @param node - a pointer to the root of the BST
 * ----------------------------------------------------------
 */
void printPostorderBST(BSTnode* nodePtr);


/** ----------------------------------------------------------
 * displayBST() displays a binary tree upto 6 levels
 * @param node - a pointer to the root of the BST
 * ----------------------------------------------------------
 */
void displayBST(BSTnode* node);
#endif //USAFA_CS220_S18_STUDENT_BSTASLINKEDDS_H
