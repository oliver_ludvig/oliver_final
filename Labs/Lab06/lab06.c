/** lab06.c
* ===========================================================
* Name: first last
* Section: n/a
* Project: Lab 6
* ===========================================================
*
* Introduction: Over the next few lessons we will be creating a
* simple cadet database program...
*
* Instructions:
*    1) Complete TASKS Below
*/
#include "lab06.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

int main() {
    MyCadetInfo cadetRecs[MAXSTUDENTS];

    /* TASK 0 - Write a function, readData(), that reads cadet records
     * (MyCadetInfoStructs) from a binary file, "lab06Data.dat" into
     * an array of MyCadetInfo type
     * 0) Create a function prototype in the lab06 header
     * file, the function takes as input the the array of cadet
     * records, it returns the number of records read
     * Hint:  fread() will attempt to read nmemb elements from the file
     * if it cannot due to reaching the end of file it returns the
     * actual number read successfully prior to the end of file being reached
     * Note: MAXSTUDENTS for nmemb makes great sense
     * see:  https://www.tutorialspoint.com/c_standard_library/c_function_fread.htm
     * 1) Call your function below and verify that it works
     */


    int amountRecords = readData(cadetRecs);
    printf("Read %d records\n", amountRecords);

    /* TASK 1 - Write a function, findIndex(), that finds the index of
     * a cadet given the cadets name
     * 0) Create a prototype in the header file, this function returns the
     * index of the requested cadet or -1 if it cannot find the cadet, it
     * takes as input the array of cadet records, the number of records,
     * and the name of the cadet whose index you wish to find.
     * 1) What is the Big-O of this function?
     *          Answer: O(n)
     * 2) Call your function below and verify that it works
     */

    /* TASK 2 - Write a function, removeRec(), that removes a cadet from
     * the database (both the internal struct and the file on disk)
     * 0) Create a prototype in the header file, this function returns
     * the number of records removed on success or -1 on failure, it takes
     * as input as input the array of cadet records, the number of records,
     * and the name of the cadet who you want to remove from the database
     * STOP and think about the easiest option for accomplishing this...
     * what functions can I re-use that are already done as part of lab 6
     * 1) Call your function below
     * 2) Test it for removing a cadets in the first, middle, last positions
     * in the database
     * Help: Use the function from TASK 1
     */

    return 0;
}

int findIndex(MyCadetInfo cadetRecords[], int numCadets, char name) {

}

/** ----------------------------------------------------------
 * readCadetBlk() is used to read a MyCadetInfo typed record
 * from a file
 * @param location is the element number of where the cadet
 * appears in the file / array of structs
 * @return number of records read
 * ----------------------------------------------------------
 */
MyCadetInfo readCadetBlk(int location) {

    // Open an data file for reading
    FILE *in = fopen("../Labs/Lab06/lab06Data.dat", "r");
    if (in == NULL) {
        printf("Error opening data file: %s.\n", strerror(errno));
        exit(1);
    }

    MyCadetInfo tempCadet;

    fseek(in, sizeof(MyCadetInfo) * (long) location, SEEK_SET);
    fread(&tempCadet, sizeof(MyCadetInfo), 1, in);
    fclose(in);
    return tempCadet;
}

/** ----------------------------------------------------------
 * writeDataBlk() is used to the entire cadet record array
 * to a binary file
 * @param cadetRecords is the array of cadet records
 * @param numCadets is the total number of cadets to write
 * @return number of records wrote
 * ----------------------------------------------------------
 */
int writeDataBlk(MyCadetInfo cadetRecords[], int numCadets) {

    // Open an output file for writing
    FILE *out = fopen("../Labs/Lab06/lab06Data.dat", "w");
    if (out == NULL) {
        printf("Error creating data file: %s.\n", strerror(errno));
        exit(1);
    }

    size_t retVal = fwrite(cadetRecords, sizeof(MyCadetInfo), numCadets, out);
    fclose(out);
    return retVal;

}

/** ----------------------------------------------------------
 * writeCadetBlk() is used to write a single cadet records
 * to a binary file the previous contents of the file are
 * not changed
 * @param cadet is the address of the cadet structure to write
 * @param location is the offset from the from of the file to
 *                 write the record to
 * @return number of records wrote
 * ----------------------------------------------------------
 */
int writeCadetBlk(MyCadetInfo *cadet, int location) {

    // Open an output file for writing
    FILE *out = fopen("../Labs/Lab06/lab06Data.dat", "r+");
    if (out == NULL) {
        printf("Error creating data file: %s.\n", strerror(errno));
        exit(1);
    }
    fseek(out, sizeof(MyCadetInfo) * (long) location, SEEK_SET);
    int retVal = fwrite(cadet, sizeof(MyCadetInfo), 1, out);
    fclose(out);
    return retVal;
}

/** ----------------------------------------------------------
 * printCadetInfo() is used to print a MyCadetInfo typed variable
 * to the console
 * @param cadetRecord is the cadet info struct to be printed
 * @return n/a
 */
void printCadetInfo(MyCadetInfo cadetRecord) {
    printf("Cadet name = \t%s\n", cadetRecord.name);
    printf("Cadet age = \t%d\n", cadetRecord.age);
    printf("Cadet squad = \t%d\n", cadetRecord.squad);
    printf("Cadet year = \t%d\n\n", cadetRecord.classYear);
}

/** ----------------------------------------------------------
 * printData() is used to print MyCadetInfo typed records
 * from a file
 * @param datums is the array of cadet records
 * @param numCadets is the number of cadets in datums
 * @return n/a
 */
void printData(MyCadetInfo *datums, int numCadets) {
    for (int i = 0; i < numCadets; i++) {
        printCadetInfo(datums[i]);
    }
}