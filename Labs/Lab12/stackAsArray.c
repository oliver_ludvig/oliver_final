//
// Created by C20Ludvig.Oliver on 2/6/2018.
//

#include "stackAsArray.h"
#include <stdio.h>
#include <string.h>

int stackPush(StackAsArray* stack, char element) {
    if (stackIsFull(*stack) == true) {
        printf("ERROR: Stack is full, unable to push element");
        return -1;
    }
    else {
        stack -> letter[stack -> top] = element;
        stack -> top = stack -> top + 1;
        return 1;
    }
}

char stackPop(StackAsArray* stack) {
    char pop = stack -> letter[stack -> top];
    stack -> top = stack -> top - 1;
    return pop;
}

bool stackIsEmpty(StackAsArray stack) {
    if (stack.top < 0)
        return true;
    else
        return false;
}

bool stackIsFull(StackAsArray stack) {
    if (stack.top == STACK_MAX_SIZE - 1)
        return true;
    else
        return false;
}

void stackInit(StackAsArray* stack) {
    stack -> top = -1;
}

char stackPeek(StackAsArray stack) {
    if (stackIsEmpty(stack) == true)
        return -1;
    else {
        char peek = stack.letter[stack.top];
        return peek;
    }

}

int stackSize(StackAsArray stack) {
    if (stackIsEmpty(stack) == true)
        return 0;
    else {
        if (stackIsFull(stack) == true)
            return STACK_MAX_SIZE;
        else {
            return stack.top;
        }
    }
}

void stackPrint(StackAsArray stack) {
    for (int i = 0; i < sizeof(stack); i++) {
        printf("%c", stack.letter[i]);
    }

}