//
// Created by C20Ludvig.Oliver on 2/12/2018.
//

/** lab14.h
* ================================================================
* Name: Ludvig Oliver
* Section: n/a
* Project: Lab14 - Pointer use in C
* =================================================================
*
* Instructions: For this lab you can borrow code from prior labs to
* accomplish the tasks below
*
*/

#ifndef USAFA_CS220_S18_STUDENT_LAB14_H
#define USAFA_CS220_S18_STUDENT_LAB14_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10

void fillArray(int array[]);

void print_array(int *array);

void add_array(int *array, int num);

void insertSort(int array[]);

void swap(int* x, int* y);

#endif //USAFA_CS220_S18_STUDENT_LAB14_H
