/** lab14.c
* ================================================================
* Name: Ludvig Oliver
* Section: n/a
* Project: Lab14 - Pointer use in C
* =================================================================
*
* Instructions: For this lab you can borrow code from prior labs to
* accomplish the tasks below
*
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "lab14.h"

/* Before you start:  you cannot use the [] operators anywhere
 * in this lab with the exception of declaring the array of integers
 * you must use ptr math and the & * operators to accomplish the tasks
 * below.  Function prototypes should be in the appropriate file.  You
 * may find additional parameters to the functions below to
 * be useful...I don't give you everything you may need.
 *
 * task 0 - create a *.h file to accompany this *.c file and modify it
 * as you progress through the tasks below
 * task 1 - create a main function in this *.c file
 * task 1a - call and test each of the functions as you complete the
 * remaining tasks
 * task 2 - write a function that generates random integer values and
 * assigns them to an array when the address of the array is passed in
 * task 4 - write a function that prints the values in the array when the
 * address of the array is passed to the function
 * task 5 - write a function that adds a given amount to each
 * item in the array when the address of the array is passed to the function
 * task 6 - write insertion sort given the address to the array
 */

int main() {
    int array[N];

    fillArray(array);

    return 0;
}

void fillArray(int *array) {
    //set up for and then seed random number
    //generator
    static int seedDone = 0; //static variables retain their value between calls

    // modified so the seed is only done once
    if (!seedDone) {
        struct timespec time;
        clock_gettime(CLOCK_REALTIME, &time);
        srandom((unsigned) (time.tv_nsec ^ time.tv_sec));
        seedDone = 1;
    }

    // fill array with random ints from 0 to 99
    for (int i = 0; i < N; i++) {
        *(array + i) = (int) random() % 100;
    }
    print_array(array);

    add_array(array, 5);
}

void add_array(int *array, int num) {
    printf("\n");
    for (int i = 0; i < N; i++) {
        *(array + i) += num;
    }
    print_array(array);

    insertSort(array);
}

void insertSort(int *array) {
    printf("\n");
    for (int i = 0; i < N; i++) {
        for (int x = 0; x < N; x++) {
            if (*(array + i) < *(array + x))
                swap(&*(array + i), &*(array + x));
        }
    }
    print_array(array);
}

void print_array(int *array) {
    for (int i = 0; i < N; i++) {
        printf("%d \n", *array++);
    }

}

void swap(int* x, int* y) {
    int temp;

    temp = *y;
    *y = *x;
    *x = temp;
}