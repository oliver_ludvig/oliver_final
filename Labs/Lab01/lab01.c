/** lab01.c
* ===========================================================
* Name: Ludvig Oliver
* Section: T3/T4
* Project: Lab 1 - Basic C Functionality
* Purpose: Learn C basics
* ===========================================================
* Instructions:
 * Complete the tasks below without using CLion!
 * Use notepad and compilation from the Cygwin command shell (bash).
 *
 * From a Cygwin command shell type "gcc filename.c -o lab01.exe" to compile
 * and link your program.
 *
 * Use ./lab01.exe to execute your program
*/
// Includes go here

#include <stdio.h>
#include <stdlib.h>
#include<time.h>

int main() {

    /* TASK A - Variable Basics
     * 0) Don't for get that you will need to add #include
     *    directives for any libraries you wish to use.
     * 1) prompt the user for a two integers x and y
     * 2) output the first value divided by the second value
     * 3) are you surprised by the output? (hopefully not)
     * 3) declare a float variable named quotient that is the
     *    first integer divided by the second integer value
     * 3) output quotient
     *    Note: Be sure to full precision division!  Do this by
     *    type converting an int to a float
     *    use -> (float) intVar (this is called a cast)
     */

int x, y;
	// IO from the console/keyboard
printf("Enter Value for x: ");
scanf("%d", &x);

printf("Enter Value for y: ");
scanf("%d", &y);

printf("When %d is divided by %d the result is %f \n", x, y, (float)x/y);

    /* TASK B - Conditional Operator
     * 1) prompt user for two values x, y
     * 2) Use the conditional operator (? :) to print x if
     *    it is larger than y otherwise print y
     */

printf("\n");
	// IO from the console/keyboard
printf("Enter Value for x: ");
scanf("%d", &x);

printf("Enter Value for y: ");
scanf("%d", &y);

printf("Largest Value is: %d \n", (x > y)? x : y);

    /* TASK C - Print divisors
     * 1) ask user for a lowerBound and an upperBound
     * 2) from the lower to the upper bound print out
     *    the multiples of 3 using a for loop
     * 3) accomplish part 2 again using a while loop and then a
     *    do while loop
     */

int lowerBound, upperBound, i;
	// IO from the console/keyboard
printf("\n");
printf("Select lowerBound: ");
scanf("%d", &lowerBound);

printf("Select upperBound: ");
scanf("%d", &upperBound);

	// multiples of 3 using a for loop
printf("\nfor loop\n");
for (i = lowerBound; i < upperBound + 1; ++i)
{
  if (i % 3 == 0)
    {
     printf("%d ", i);
    }
}

	// multiples of 3 using a while loop
printf("\n");
printf("\nwhile loop\n");
i = lowerBound;
while (i < upperBound + 1)
{
   if (i % 3 == 0)
    {
     printf("%d ", i);
    }
   ++i;
}

	// multiples of 3 using a do while loop
printf("\n");
printf("\ndo while loop\n");
i = lowerBound;
do
{
   if (i % 3 == 0)
    {
     printf("%d ", i);
    }
   ++i;
}
while (i < upperBound + 1);

    /* TASK D - Grade Scale
     * 1) prompt the user for an integer grade value 4=A, 3=B and so on
     * 2) use a switch statement to output the corresponding letter grade
     *    Note: use default case for "F"
     */

int grade;
printf("\n");

	// IO from console/keyboard
printf("\nInput Your Integer Grade: ");
scanf("%d", &grade);

switch (grade)
{
   case 4:
      printf("You earned an A");
    break;

   case 3:
      printf("You earned an B");
    break;

   case 2:
      printf("You earned an C");
    break;

   case 1:
      printf("You earned an D");
    break;

   default:  
      printf("You earned an F");
}


    /* TASK E - Guessing game in C
     * 1) generate a random integer in c, the magic number
     *          * rand() in stdlib.h may be useful
     * 2) prompt the user for an integer between 1 and 100 this will
     *    be the users guess of the magic number
     * 3) print appropriate messages if the guess is low or high relative
     *    to the magic number and allow the user to guess again
     * 4) if the user guesses the magic number output the number of guesses
     *    made -- game over
     */

//time_t t;
srand((unsigned) time(0));
int magicNumber = rand() % 100 + 1, guess, counter;
printf("\n");
guess = 0;
counter = 1;

while (guess != magicNumber)
{
   printf("\nTake a Guess between 1 - 100: ");
   scanf("%d", &guess);

   if (guess < magicNumber)
    {
     printf("\nGuess is too low, try something higher");
    }
   if (guess > magicNumber)
    {
     printf("\nGuess is too high, try something lower");
    }
   if (guess == magicNumber)
    {
     printf("\nGreat Job! The Magic Number is: %d, it took you %d attempts \n", magicNumber, counter);
    }

   ++counter;

}

}