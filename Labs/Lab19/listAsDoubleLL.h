/** listAsDoubleLL.h
* ================================================================
* Name: first last
* Section: xxx
* Project: Lab19 - List ADT w/Doubly Linked List
* =================================================================
*
* Instructions:  Implement the functionality of your linked list library
* using a doubly linked list.  Reexamine the typedefs / functions /
* their parameters / their return types.  You should also create a lab19.c
* file that tests your doubly linked list library.  At the end of this lab
* you should have three files, lab19.c (to test the library), this header
* file, and listAsDouble.c (implementation file).
*/
#ifndef USAFA_CS220_S18_STUDENT_LISTASDOUBLELL_H
#define USAFA_CS220_S18_STUDENT_LISTASDOUBLELL_H





#endif //USAFA_CS220_S18_STUDENT_LISTASDOUBLELL_H
