/** listAsLinkedList.c
* ================================================================
* Name: Ludvig Oliver
* Section: T3/T4
* Project: Header for Linked List Library used in PEX2
* =================================================================
*
* Documentation:  I used lab 16 that I completed as a reference
*                 for this portion of the PEX.
*
*/

#include <stdio.h>
#include <stdlib.h>
#include "listAsLinkedList.h"


LinkedList *createLinkedList() {
    LinkedList *newList = malloc(sizeof(LinkedList));
    newList->head = NULL;
    newList->tail = NULL;
    newList->numberOfElements = 0;
    return newList;
}

void deleteLinkedList(LinkedList *list) {
    Node *tempPtr = list->head;
    Node *freePtr = list->head;
    while (tempPtr != NULL) {
        tempPtr = tempPtr->next;
        free(freePtr);
        freePtr = tempPtr;
    }
    list->head = NULL;
    list->tail = NULL;
    list->numberOfElements = 0;
}

void appendElementLinkedList(LinkedList *list, Segment element) {
    Node *newNode = malloc(sizeof(Node));  // Creates memory space of node
    newNode->segment.x = element.x;       // made data into desired element
    newNode->segment.y = element.y;
    newNode->next = NULL;
    if (list->numberOfElements == 0) {
        list->head = newNode;
        list->tail = newNode;
        list->numberOfElements += 1;
    } else {
        list->tail->next = newNode;
        list->tail = newNode;       // the tail is now the newNode
        list->numberOfElements += 1;  // adds 1 to the element
    }
}

int lengthOfLinkedList(LinkedList *list) {
    return list->numberOfElements;
}

void printLinkedList(LinkedList *list) {
    Node *tempPtr = list->head;
    while (tempPtr != NULL) {
        printf("(%d, %d)\n", tempPtr->segment.x, tempPtr->segment.y);
        tempPtr = tempPtr->next;
    }
    printf("NULL\n");
}

Segment getElementLinkedList(LinkedList *list, int position) {
    if (list->numberOfElements == 0 || position > list->numberOfElements) {
        if (list->numberOfElements == 0) {
            printf("Error // Empty List");
            exit(-1);
        } else {
            printf("Error // Position is not in the list");
            exit(-1);
        }
    } else {
        int place = 0;
        Node *tempPtr = list->head;
        while (tempPtr != NULL) {
            if (place == position) {
                return tempPtr->segment;
            }
            tempPtr = tempPtr->next;
            place += 1;
        }
    }
    exit(-1);
}

void changeElementLinkedList(LinkedList *list, int position, Segment newElement) {
    if (list->numberOfElements == 0) {
        printf("Error // Empty List");
        return;
    }
    int place = 0;
    Node *tempPtr = list->head;
    while (tempPtr != NULL) {
        if (place == position) {
            tempPtr->segment = newElement;
            return;
        }
        tempPtr = tempPtr->next;
        place += 1;
    }
}

void deleteElementLinkedList(LinkedList *list, int position) {
    if (position > list->numberOfElements || list->numberOfElements == 0) {
        if (position > list->numberOfElements) {
            printf("Error // Position is not in the list");
        } else {
            printf("Error // Empty List");
        }
        return;
    } else if (position == 0) {
        Node *tempPtr = list->head;
        list->head = tempPtr->next;
        free(tempPtr);
        list->numberOfElements--;
    } else if (position == list->numberOfElements) {
        Node *tempPtr = list->head;
        int i = 0;
        while (i < position - 1) {
            tempPtr = tempPtr->next;
            i++;
        }
        list->tail = tempPtr;
        list->tail->next = NULL;
        free(tempPtr->next);
        list->numberOfElements--;
    } else {
        int i = 0;
        Node *tempPtr = list->head;
        while (i < position - 1) {
            i++;
            tempPtr = tempPtr->next;
        }
        Node *freePtr = tempPtr->next;
        tempPtr->next = freePtr->next;
        free(freePtr);
        list->numberOfElements--;
    }
}

void insertElementLinkedList(LinkedList *list, int position, Segment element) {
    if (position > list->numberOfElements) {
        return;
    }
    // Create New Node
    Node *newNode = malloc(sizeof(Node));
    newNode->segment = element;
    newNode->next = NULL;
    // insert node based on cases
    if (list->numberOfElements == 0) {
        list->head = newNode;
        list->tail = newNode;
        list->numberOfElements++;
    } else if (position == 0) {
        newNode->next = list->head;
        list->head = newNode;
        list->numberOfElements++;
    } else if (position == list->numberOfElements + 1) {
        appendElementLinkedList(list, element);
    } else {
        Node *tempPtr = list->head;
        int i = 0;
        while (i < position - 1) {
            i++;
            tempPtr = tempPtr->next;  // tempPtr is pointing to the new node previously
        }
        newNode->next = tempPtr->next;
        tempPtr->next = newNode;
        list->numberOfElements++;
    }
}

int findElementLinkedList(LinkedList *list, Segment element) {
    Node *tempPtr = list->head;
    int size = list->numberOfElements;
    int i = 0;
    while (i < size) {
        if (tempPtr->segment.x == element.x && tempPtr->segment.y == element.y) {
            return i;
        }
        tempPtr = tempPtr->next;
        i++;
    }
    return -1;
}
