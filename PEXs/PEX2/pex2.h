/** pex2.h
* ===========================================================
* Name: Ludvig Oliver
* Section: T3/T4
* Project: PEX2 - Test linked list with Segment
* ===========================================================
*
* PEX2 - Snake Header File
*
* Documentation:  I received no help on this portion of
* the assignment.
*
*/

#ifndef USAFA_CS220_S18_STUDENT_PEX2_H
#define USAFA_CS220_S18_STUDENT_PEX2_H



#include <ncurses.h>
#include <curses.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "listAsLinkedList.h"

#define world_width 50
#define world_height 20

#define rate 100

enum direction { UP, DOWN, RIGHT, LEFT };


/** ------------------------------------------------------------------
 * move_snake() uses the linked list to print the data and also
 * update the information of the body and head for the new position
 * of the snake
 * @param world : Window to be manipulated for the snake
 * @param cur_dir : Current direction of the snake
 * @param snake_list : The Linked List for the snake information
 * @param food : The position of the food to be printed on the screen
 * @return : Nothing or 0
 *  ------------------------------------------------------------------
 */
int move_snake(WINDOW *world, int cur_dir, LinkedList *snake_list, Segment food);


/** ------------------------------------------------------------------
 * check_loss() uses the linked list to check if the head is on the
 * borders or has eaten itself to end the game
 * @param snake_list : Uses the snake list to check any possible game
 * ending positions
 * @retur : True or False of wheter the game is over
 *  ------------------------------------------------------------------
 */
bool check_loss(LinkedList *snake_list);


/** ------------------------------------------------------------------
 * create_food() uses the linked list to ensure that the generated
 * random position of the food is not on the snake as well as on the
 * boders -- overall creates the position of the food
 * @param snake_list : Linked List of the snake to verify its location
 * @return : {x , y} position of where the food will be
 *  ------------------------------------------------------------------
 */
Segment create_food(LinkedList *snake_list);


/** ------------------------------------------------------------------
 * eat_food() is a function used to check if the snake head has eaten
 * the randomly populated food.
 * @param snake_list : Linked List to reference its position
 * @param food : Segment of where the food is located
 * @return : {x , y} position of where the new food will be if eaten
 *  ------------------------------------------------------------------
 */
Segment eat_food(LinkedList *snake_list, Segment food);


/** ------------------------------------------------------------------
 * check_body() is a function that checks if the snake has eaten itself
 * -- this is used in check_loss
 * @param snake_list : Linked List to reference the head to the body
 * @return : True or False of whether the game is over
 *  ------------------------------------------------------------------
 */
bool check_body(LinkedList *snake_list);


#endif //USAFA_CS220_S18_STUDENT_PEX2_H