/** listAsLinkedList.h
* ================================================================
* Name: Ludvig Oliver
* Section: M3/M4
* Project: Header for Linked List Library used in PEX2
* =================================================================
*/
#ifndef USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
#define USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H

#include <curses.h>
#include <stdbool.h>

// Define the type for a segment of the snake
typedef struct {
    int x;  // the x location of the segment
    int y;  // the y location of the segment
} Segment;

// Define type for a node of the linked list
typedef struct node {
    Segment segment;  // our list now contains a segment
    struct node* next;
} Node;

// Define the type (meta-data) that manages the linked list of nodes
typedef struct {
    Node* head;
    Node* tail;
    int numberOfElements;
} LinkedList;

/** ----------------------------------------------------------
 * createLinkedList() allocates and initializes the linked list
 * meta data
 * @return returns a pointer to the allocated linked list meta
 * data
 * ----------------------------------------------------------
 */
LinkedList* createLinkedList();


/** ----------------------------------------------------------
 * deleteLinkedList() frees all memory associated with the linked
 * list to include the meta data and all the nodes in the list
 * @param list - a pointer to the linked list
 * ----------------------------------------------------------
 */
void deleteLinkedList(LinkedList* list);


/** ----------------------------------------------------------
 * appendElementLinkedList() appends an element to the list
 * @param list - pointer to the linked list
 * @param element - the element to append
 * ----------------------------------------------------------
 */
void appendElementLinkedList(LinkedList* list, Segment element);


/** ----------------------------------------------------------
 * lengthOfLinkedList() returns the length of the linked list
 * @param pointer to the linked list
 * @return the length of the linked list of nodes
 * ----------------------------------------------------------
 */
int lengthOfLinkedList(LinkedList* list);


/** ----------------------------------------------------------
 * printLinkedList() prints the linked list of nodes
 * @param list - a pointer to the linked list
 * ----------------------------------------------------------
 */
void printLinkedList(LinkedList* list);

/** ----------------------------------------------------------
 * getElementLinkedList() gets an element from the linked list
 * it doesn't remove the element just returns it
 * @param list - a pointer to the linked list
 * @param position - the position in the list 0..length-1
 * @return the requested element of the list
 * ----------------------------------------------------------
 */
Segment getElementLinkedList(LinkedList* list, int position);


/** ----------------------------------------------------------
 * changeElementLinkedList() changes an element in the linked list
 * @param list - a pointer to the linked list
 * @param position - the position in the list 0..length-1
 * @param newElement - the new data to place into the list over writing
 * the element in the list at position
 * ----------------------------------------------------------
 */
void changeElementLinkedList(LinkedList* list, int position, Segment newElement);


/** ----------------------------------------------------------
 * deleteElementLinkedList() deletes an element from the linked list
 * @param list - a pointer to the linked list
 * @param position - the position in the list 0..length-1
 * ----------------------------------------------------------
 */
void deleteElementLinkedList(LinkedList* list, int position);


/** ----------------------------------------------------------
 * insertElementLinkedList() inserts an element into the linked list
 * the inserted element is inserted at the position given (if you
 * insert at position n the new element is at position n after the
 * insert)
 * @param list - a pointer to the linked list
 * @param position - the position in the list 0..length-1
 * @param element - the element to insert into the list
 * ----------------------------------------------------------
 */
void insertElementLinkedList(LinkedList* list, int position, Segment element);


/** ----------------------------------------------------------
 * findElementLinkedList() finds the position of a given element
 * in the linked list
 * @param list - a pointer to the linked list
 * @param element - the element to find in the linked list
 * @return position of the element or -1 if not found
 * ----------------------------------------------------------
 */
int findElementLinkedList(LinkedList* list, Segment element);


#endif //USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
