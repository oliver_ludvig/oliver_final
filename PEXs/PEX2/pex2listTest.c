/** pex2listTest.c
* ===========================================================
* Name: Ludvig Oliver
* Section: T3/T4
* Project: PEX2 - Test linked list with Segment
* ===========================================================
*
* Instructions:  Use this file to test your linked list library
* implementations (listAsLinkedList.c).
*
*/

#include <stdio.h>
#include <stdlib.h>
#include "listAsLinkedList.h"

int main() {
/** MULTI-ELEMENT LIST - list operations with a SINGLE element list
 */
    printf("-----------------MULTI--------------------------\n"); // divider for test sections
    // create a 5 element linked list of segments
    LinkedList *snakeList4 = createLinkedList();
    Segment tempSeg = {0, 0};

    // append test
    for (int i = 0; i < 5; i++) {
        tempSeg.x = i;
        tempSeg.y = i + 1;
        appendElementLinkedList(snakeList4, tempSeg);
    }
    printLinkedList(snakeList4); // result: (0,1) -> (1,2) -> (2,3) -> (3,4) -> (4,5) -> NULL

    printf("\n");
    // insert test
    tempSeg.x = 17;
    tempSeg.y = 17;
    insertElementLinkedList(snakeList4, 0, tempSeg); // head, pos = 0
    insertElementLinkedList(snakeList4, 5, tempSeg); // tail, pos = len list -1
    insertElementLinkedList(snakeList4, 3, tempSeg); // interior, pos = interior index
    printLinkedList(
            snakeList4); // result: (17,17) -> (0,1) -> (1,2) -> (17,17) -> (2,3) -> (3,4) -> (17,17) -> (4,5) -> NULL

    printf("\n");
    // find test
    printf("found %d\n", findElementLinkedList(snakeList4, tempSeg)); // head, result: found = 0
    tempSeg.x = 4;
    tempSeg.y = 5;
    printf("found %d\n", findElementLinkedList(snakeList4, tempSeg)); // tail, result: found = 7
    tempSeg.x = 2;
    tempSeg.y = 3;
    printf("found %d\n", findElementLinkedList(snakeList4, tempSeg)); // interior, result: found = 4
    tempSeg.x = 99;
    tempSeg.y = 99;
    printf("found %d\n", findElementLinkedList(snakeList4, tempSeg)); // not in list, result: found = -1

    printf("\n");
    // change test
    changeElementLinkedList(snakeList4, 0, tempSeg); // head
    changeElementLinkedList(snakeList4, 7, tempSeg); // tail
    changeElementLinkedList(snakeList4, 4, tempSeg); // interior
    printLinkedList(
            snakeList4); // result: (99,99) -> (0,1) -> (1,2) -> (17,17) -> (99,99) -> (3,4) -> (17,17) -> (99,99) -> NULL

    printf("\n");
    // length test
    printf("length = %d\n", lengthOfLinkedList(snakeList4)); // result: length = 8

    printf("\n");
    // delete element
    deleteElementLinkedList(snakeList4, 0); // head
    deleteElementLinkedList(snakeList4, 6); // tail
    deleteElementLinkedList(snakeList4, 3); // interior
    printLinkedList(snakeList4); // result: (0,1) -> (1,2) -> (17,17) -> (3,4) -> (17,17) -> NULL

    printf("\n");
    // get element
    Segment retrElem = getElementLinkedList(snakeList4, 0); // head
    printf("get element = {%d, %d}\n", retrElem.x, retrElem.y); // result: get element = {0, 1}
    retrElem = getElementLinkedList(snakeList4, 4); // tail
    printf("get element = {%d, %d}\n", retrElem.x, retrElem.y); // result: get element = {17, 17}
    retrElem = getElementLinkedList(snakeList4, 3); // interior
    printf("get element = {%d, %d}\n", retrElem.x, retrElem.y); // result: get element = {3, 4}

    printf("\n");
    // delete linked list
    deleteLinkedList(snakeList4);
    printf("-----------------SINGLE--------------------------\n"); // divider for test sections

/** SINGLE ELEMENT LIST - list operations with a SINGLE element list
 */
    // create single element list
    LinkedList *snakeList3 = createLinkedList();
    Segment singleElem = {5, 7};
    appendElementLinkedList(snakeList3, singleElem);

    // delete test
    deleteElementLinkedList(snakeList3, 0);
    printLinkedList(snakeList3); // result: NULL
    appendElementLinkedList(snakeList3, singleElem); // re-append deleted element

    // insert test
    Segment newElem = {10, 17};
    insertElementLinkedList(snakeList3, 0, newElem);
    printLinkedList(snakeList3); // result: (10,17) -> (5,7) -> NULL
    deleteElementLinkedList(snakeList3, 0); // remove inserted element list

    // change test
    Segment newerElem = {22, 27};
    changeElementLinkedList(snakeList3, 0, newerElem);
    printLinkedList(snakeList3); // result: (22,27) -> NULL

    // length test
    printf("length = %d\n", lengthOfLinkedList(snakeList3)); // result: length = 1

    // get test
    retrElem = getElementLinkedList(snakeList3, 0);
    printf("get element = {%d, %d}\n", retrElem.x, retrElem.y); // result: get element = {22, 27}

    // append test
    Segment newElem2 = {33, 77};
    appendElementLinkedList(snakeList3, newElem2);
    printLinkedList(snakeList3); // result: (22,27) -> (33,77) -> NULL

    // delete linked list
    deleteLinkedList(snakeList3);
    printf("----------------EMPTY---------------------------\n"); // divider for test sections

/** EMPTY LIST - list operations with an empty list
*/
    // create a list to hold the segments of the snake
    LinkedList *snakeList = createLinkedList();

    // print test
    printLinkedList(snakeList); // result: should print: NULL

    // find test
    Segment headSnake = {20, 20};
    printf("found = %d\n", findElementLinkedList(snakeList, headSnake)); // result: found = -1

    // length test
    printf("length = %d\n", lengthOfLinkedList(snakeList)); // result: length = 0

    // append test
    appendElementLinkedList(snakeList, headSnake);
    printLinkedList(snakeList); // result: (20,20) -> NULL

    // insert test
    LinkedList *snakeList2 = createLinkedList();
    insertElementLinkedList(snakeList2, 0, headSnake);
    printLinkedList(snakeList2); // result: (20,20) -> NULL

/** delete, change, and get tests
* you will need to uncomment each of the lines below
* separately, as each independently should cause the program
* to print an error message and exit
*/
    printf("----------------ERROR/EXIT---------------------------\n");
    LinkedList* snakeList1 = createLinkedList();
    deleteElementLinkedList(snakeList1,0); // result: should print error message then exit()
    printf("\n");

    Segment newHead = {57,57};
    changeElementLinkedList(snakeList1,0,newHead); // result should print error message and exit()
    printf("\n");

    Segment theSeg = getElementLinkedList(snakeList1,0); // result: should print error message then exit()
    printf("%d, %d", theSeg.x, theSeg.y);

    return 0;
}

