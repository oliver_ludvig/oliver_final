/** pex2.c
* ===========================================================
* Name: Ludvig Oliver
* Section: T3/T4
* Project: PEX2 - Test linked list with Segment
* ===========================================================
*
* PEX2 - Snake File
*
* Documentation:  I received help from C3C Matthew Kuhn on
* figuring out how to identify if the user pressed a different
* button to exit the game (other than the arrow keys).  I also
* referenced the ncurses - Zoned Out website as a reference
* for majority of my code in order to understand and make the
* snake game work.  (https://rhardih.io/tag/ncurses/)
*
*/

#include "pex2.h"

int main() {

    // Initialize Window
    WINDOW *world;
    int offsetX;
    int offsetY;

    initscr();
    noecho();
    cbreak();
    timeout(rate);
    keypad(stdscr, TRUE);

    refresh();

    offsetX = (COLS - world_width) / 2;
    offsetY = (LINES - world_height) / 2;

    world = newwin(world_height, world_width, offsetY, offsetX);



    // Initialize Snake
    LinkedList *snake_list = createLinkedList();

    int begX = (world_width) / 2;
    int begY = (world_height) / 2;

    Segment beg_seg = {begX, begY};

    appendElementLinkedList(snake_list, beg_seg);

    // Initialize Food
    Segment food = create_food(snake_list);

    // Initialize Movement
    int game_over = FALSE;
    int cur_dir = RIGHT;
    int press;


    while (game_over != TRUE) {

        move_snake(world, cur_dir, snake_list, food);

        game_over = check_loss(snake_list);  // Checks borders to end game

        food = eat_food(snake_list, food);  // Generates food for the game

        if ((press = getch()) != ERR) {
            switch (press) {
                case KEY_UP:
                    if (cur_dir == DOWN) {
                        cur_dir = DOWN;
                    } else {
                        cur_dir = UP;
                    }
                    break;
                case KEY_DOWN:
                    if (cur_dir == UP) {
                        cur_dir = UP;
                    } else {
                        cur_dir = DOWN;
                    }
                    break;
                case KEY_LEFT:
                    if (cur_dir == RIGHT) {
                        cur_dir = RIGHT;
                    } else {
                        cur_dir = LEFT;
                    }
                    break;
                case KEY_RIGHT:
                    if (cur_dir == LEFT) {
                        cur_dir = LEFT;
                    } else {
                        cur_dir = RIGHT;
                    }
                    break;
                default:
                    game_over = TRUE;
                    break;
            }
        }
    }

    wclear(world);

    // Closing Screen
    box(world, 0, 0);
    mvwprintw(world, 8, 19, "Game Over...");
    mvwprintw(world, 9, 21, "Score:%d", snake_list->numberOfElements - 1);
    mvwprintw(world, 11, 13, "Press Any Button to Close");
    wrefresh(world);

    while (getch() == ERR) {
    }

    deleteLinkedList(snake_list);

    delwin(world);

    endwin();

    return 0;

}

int move_snake(WINDOW *world, int cur_dir, LinkedList *snake_list, Segment food) {

    wclear(world);

    // Keeps track of the previous position to move the snake in a snakey manner
    if (snake_list->numberOfElements > 1) {

        Segment prev_pos = getElementLinkedList(snake_list, 0);  // Holds the head position
        Node *tempPtr = snake_list->head;
        int i = 0;

        while (tempPtr->next != NULL) {

            Segment next_pos = getElementLinkedList(snake_list, i + 1);  // Holds the position of the body

            tempPtr = tempPtr->next;

            tempPtr->segment.x = prev_pos.x;
            tempPtr->segment.y = prev_pos.y;

            mvwaddch(world, tempPtr->segment.y, tempPtr->segment.x, 'o'); // Prints the body of the snake

            prev_pos = next_pos; // Allows for a switch and update to occur

            i++;
        }
    }

    mvprintw(0, 0, "The Snake Game -- Select Any Button to Exit");

    mvprintw(1, 0, "Score:%d", snake_list->numberOfElements - 1);

    // Switches direction and updates the heads position
    switch (cur_dir) {
        case UP:
            snake_list->head->segment.y--;
            break;
        case DOWN:
            snake_list->head->segment.y++;
            break;
        case LEFT:
            snake_list->head->segment.x--;
            break;
        case RIGHT:
            snake_list->head->segment.x++;
        default:
            break;
    }

    Node *tempSeg = snake_list->head;

    // Prints head
    mvwaddch(world, tempSeg->segment.y, tempSeg->segment.x, '@');

    // Prints food
    mvwaddch(world, food.y, food.x, '*');

    box(world, 0, 0);

    wrefresh(world);

    return 0;
}

bool check_loss(LinkedList *snake_list) {

    Segment snake_head = getElementLinkedList(snake_list, 0);
    int x = snake_head.x;
    int y = snake_head.y;

    // Checks to see if the snake ate itself then checks to see if it has eaten the wall
    if ((check_body(snake_list)) == TRUE) {
        return TRUE;
    } else {
        if (x == world_width && (y <= 20 && y >= 0)) {  // Right Border
            return TRUE;
        } else if (x == 0 && (y <= 20 && y >= 0)) {  // Left Border
            return TRUE;
        } else if ((x <= world_width && x >= 0) && y == 0) {  // Top Border
            return TRUE;
        } else if ((x <= world_width && x >= 0) && y == world_height) {  // Bottom Border
            return TRUE;
        } else {
            return FALSE;
        }

    }
}

Segment create_food(LinkedList *snake_list) {

    Segment food = {0, 0};

    // Allows for random generator of numbers
    struct timespec time;
    clock_gettime(CLOCK_REALTIME, &time);
    srandom((unsigned) (time.tv_nsec ^ time.tv_sec));

    // Generates new position of food
    food.x = (int) random() % 48;
    food.y = (int) random() % 18;

    for (int i = 0; i < lengthOfLinkedList(snake_list); i++) {
        Segment tempSeg = getElementLinkedList(snake_list, i);

        // Ensures the food is not populating in the body of the snake
        if (food.x == tempSeg.x && food.y == tempSeg.y) {
            food.x = (int) random() % 48;
            food.y = (int) random() % 18;
        }

        // Ensures that food is within the box and not on the line
        if (food.x == world_width || food.x == 0) {
            food.x = (int) random() % 48;
        }
        if (food.y == world_height || food.y == 0) {
            food.y = (int) random() & 18;
        }

    }
    return food;
};

Segment eat_food(LinkedList *snake_list, Segment food) {

    int eaten;

    Segment snake_head = getElementLinkedList(snake_list, 0);

    // Check to see if food has been eaten by snake
    if (snake_head.x == food.x && snake_head.y == food.y) {
        eaten = TRUE;
    } else {
        eaten = FALSE;
    }

    if (eaten == TRUE) {

        // Adds To Snake
        Segment snake_tail = getElementLinkedList(snake_list, snake_list->numberOfElements - 1);

        appendElementLinkedList(snake_list, snake_tail);

        // Creates new food
        food = create_food(snake_list);

        return food;
    } else {

        // Keeps the old food
        return food;

    }
}

bool check_body(LinkedList *snake_list) {

    Node *snake_head = snake_list->head;
    Node *tempPtr = snake_list->head->next;

    if (snake_list->numberOfElements > 1) {
        while (tempPtr->next != NULL) {

            // checks to see if the head is on any part of the body of the snake
            if (snake_head->segment.x == tempPtr->segment.x && snake_head->segment.y == tempPtr->segment.y) {
                return TRUE;
            } else {
                tempPtr = tempPtr->next;
            }

        }
    }
    return FALSE;
}