/** sort.c
* ===========================================================
* Name: Ludvig Oliver
* Section: T4/T5
* Project: PEX1 - sort.c
* Purpose: xxx
* ===========================================================
*/

//swaps two integer values
void swap(int *x, int *y) {
    int temp;

    temp = *y;
    *y = *x;
    *x = temp;
}

void selSort(int array[], int N, int order) {
    int smallest = 0;
    int small_index = 0;
    for (int i = 0; i < N; i++) {
        for (int x = 0; x < N; x++) {
            smallest = array[i];
            if (order == 1) {
                if (array[x] > smallest) {
                    smallest = array[x];
                    smallest_index = x;
                }
            } else {
                if (array[x] < smallest) {
                    smallest = array[x];
                    smallest_index = x;
                }
            }
        }
        swap(&array[i], &array[smallest_index]);
    }
}

void insertSort(int array[], int N) {
    int insert = 0;
    for (int i = 0; i < N; i++) {
        insert = array[i];
        int x = i - 1;
        while (x > 0 && array[x] > insert) {
            array[x + 1] = array[x];
            x--;
        }
        array[x + 1] = insert;
    }
}

void merge(int array[], int lBound, int mid, int rBound, int N) {
    int tempArray[N];
    // Copy the first sublist into the tempArray
    for (int j = lBound; j <= mid; j++) {
        tempArray[j] = array[j];
    }
    // Copy the second sublist into the tempArray
    for (int j = mid + 1, k = rBound; j <= rBound; j++, k--) {
        tempArray[k] = array[j];
    }

    // Merge the two sublists
    int j = lBound;
    int k = rBound;
    int i = lBound;
    while (j <= k) {
        if (tempArray[j] < tempArray[k]) {
            array[i] = tempArray[j];
            j++;
        } else {
            array[i] = tempArray[k];
            k--;
        }
        i++;
    }
}

void mergeSort(int array[], int lBound, int rBound, int N) {
    if (lBound < rBound) {
        int mid = (lBound + rBound) / 2;
        mergeSort(array, lBound, mid, N);
        mergeSort(array, mid + 1, rBound, N);
        merge(array, lBound, mid, rBound, N);
    }
}


int partition(int array[], int lBound, int rBound) {
    int pivot = array[lBound];
    int lastSmall = lBound;
    for (int i = lBound + 1; i <= rBound; i++) {
        if (array[i] < pivot) {
            lastSmall++;
            swap(&array[lastSmall], &array[i]);
        }
    }
    swap(&array[lBound], &array[lastSmall]);
    return lastSmall;  //return the division point
}

void quickSort(int array[], int lBound, int rBound) {
    if (lBound < rBound) {
        int divPt = partition(array, lBound, rBound);
        quickSort(array, lBound, divPt - 1);
        quickSort(array, divPt + 1, rBound);
    }
}
