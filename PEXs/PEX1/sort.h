/** sort.h
* ===========================================================
* Name: First Last
* Section: xxx
* Project: xxx
* Purpose: xxx
* ===========================================================
*/
#ifndef USAFA_CS220_S18_STUDENT_PEX1_H
#define USAFA_CS220_S18_STUDENT_PEX1_H

/** -------------------------------------------------------------------
 * selSort() - Perform a selection sort on an array
 * @param array - the array to sort
 * @param N - the size of the array
 * @param order - 0 for ascending, 1 for descending
 */
void selSort(int array[], int N, int order);

/** -------------------------------------------------------------------
 * insertSort() - Perform a insertion sort on an array
 * @param array - the array to sort
 * @param N - the size of the array
 */
void insertSort(int array[], int N);

void swap(int *x, int *y);

/** -------------------------------------------------------------------
 * mergeSort() - Perform a mergesort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 * @param N - the size of the array
 */
void mergeSort(int array[], int lBound, int rBound, int N);

/** -------------------------------------------------------------------
 * quickSort() - Perform a quick sort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 */

int partition(int array[], int lBound, int rBound);

void quickSort(int array[], int lBound, int rBound);


#endif //USAFA_CS220_S18_STUDENT_PEX1_H
