/** pex1.c
* ===========================================================
* Name: Ludvig Oliver
* Section: T3/T4
* Project: PEX1.c
* Purpose: xxx
* Documentation (code only):
*   The code used in sort.c and search.c was copied from the
* solutions given by Dr. Weingart.  However, sel sort and linear
* search was originially my work.  The other code was either
* referenced to create my own code or copied over.  I also received
* help from both C3C Marc Rees and C3C Noah Diamond on identifying
* errors on my selection output which was rooted to my code of selection
* sort which was fixed.  This was addressed with assistance from LtCol
* Caswell on the full implementation of the order and use of smallest
* and smallest_integer variables.
* ===========================================================
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include "sort.h"
#include "search.h"

// Local function prototypes
void fillArray(int array[], int N);

void loadData(int array[], int N, int order);

void writeData(int array[], int N, int order);

int main(int argc, char *argv[]) {


    enum ALG {
        LIN, BIN, SEL, INS, MRG, QCK
    };
    enum DATA {
        RND, ASC, DSC
    };

    // grab command line inputs to main()
    int alg = atoi(argv[1]);  // which alg to test
    int dataType = atoi(argv[2]); // what type of data
    int N = atoi(argv[3]); // size of the data set

    // check upper bound on N
    if (N > 100000000) {
        printf("Error N too large must be <= 5000000\n");
        exit(1);
    }

    // allocate space for input data
    int nums[N];

    //refine input data based on type selected
    switch (dataType) {
        // populate array with random input data
        case RND: {
            fillArray(nums, N);
            break;
        }
            // load sorted ascending data
        case ASC: {
            loadData(nums, N, 0);
            break;
        }
            // load sorted descending data
        case DSC: {
            loadData(nums, N, 1);
            break;
        }
    }

    // get a reading of the clock in ticks prior to running algorithm of choice
    clock_t startT, endT;
    startT = clock();

    // run tests for given algorithm on given datatype and size
    // the search algorithms attempt to find a value somewhat close to
    // middle
    switch (alg) {
        case LIN: {
            linearSearch(nums, N, N / 2);
            break;
        }
        case BIN: {
            binSearch(nums, 0, N - 1, N / 2);
            break;
        }
        case SEL: {
            selSort(nums, N, 0);
            break;
        }
        case INS: {
            insertSort(nums, N);
            break;
        }
        case MRG: {
            mergeSort(nums, 0, N - 1, N);
            break;
        }
        case QCK: {
            quickSort(nums, 0, N - 1);
            break;
        }
    }
    // capture end time, output results, and exit  Note: As time is in ticks you need
    // to get seconds by dividing by CLOCKS_PER_SEC
    endT = clock();
    double totalT = (double) (endT - startT) / CLOCKS_PER_SEC;
    printf("%d, %d, %d, %lf\n", alg, dataType, N, totalT);

    return 0;
}

/** ----------------------------------------------------------
 * fillArray() - fills array with random ints from 0 - N-1
 * @param array is the array of integers
 * @param N is the number of ints to load
 * ----------------------------------------------------------
 */
void fillArray(int array[], int N) {
    //set up for and then seed random number generator
    static int seedDone = 0; //static variables retain their value between calls

    // if random number generator has been seeded already don't do it again
    if (!seedDone) {
        struct timespec time;
        clock_gettime(CLOCK_REALTIME, &time);
        srandom((unsigned) (time.tv_nsec ^ time.tv_sec));
        seedDone = 1;
    }

    // fill array with random ints
    for (int i = 0; i < N; i++) {
        array[i] = (int) random() % N;
    }
}

/** ----------------------------------------------------------
 * loadData() - loads array with either ascending or descending
 * input data
 * @param array is the array of integers
 * @param N is the number of ints to load
 * @param order 0-ascending, 1-descending
 * ----------------------------------------------------------
 */
void loadData(int array[], int N, int order) {
    // Open an input file for reading
    FILE *in = NULL;
    if (order == 0) {
        in = fopen("../PEXs/PEX1/ascend.txt", "r");
    } else {
        in = fopen("../PEXs/PEX1/descend.txt", "r");
    }

    if (in == NULL) {
        printf("Error opening data file: %s.\n", strerror(errno));
        exit(1);
    }
    int numRead = 0;
    while (numRead < N && !feof(in)) {
        fscanf(in, "%d", &array[numRead]);
        numRead++;
    }
    fclose(in);
}

/** ----------------------------------------------------------
 * writeData() - writes array with either ascending or descending
 * data
 * @param array is the array of integers
 * @param N is the number of ints to write
 * @param order 0-ascending, 1-descending
 * ----------------------------------------------------------
 */
void writeData(int array[], int N, int order) {
    // Open an input file for reading
    FILE *out = NULL;
    if (order == 0) {
        out = fopen("../PEXs/PEX1/ascend.txt", "w");
    } else {
        out = fopen("../PEXs/PEX1/descend.txt", "w");
    }

    if (out == NULL) {
        printf("Error creating data file: %s.\n", strerror(errno));
        exit(1);
    }
    int numWrote = 0;
    while (numWrote < N && !feof(out)) {
        fprintf(out, "%d\n", array[numWrote]);
        numWrote++;
    }
    fclose(out);
}