/** search.c
* ===========================================================
* Name: Ludvig Oliver
* Section: M3/M4
* Project: PEX1.c
* Purpose: xxx
* ===========================================================
*/

int binSearch(int array[], int lBound, int rBound, int item) {

    if (lBound <= rBound) {

        int mid = lBound + (rBound - lBound) / 2;

        if (item == array[mid]) {
            return mid;
        } else if (array[mid] > item) {
            return binSearch(array, lBound, mid - 1, item);
        }
        return binSearch(array, rBound, mid + 1, item);

    }
    return -1;
}

int linearSearch(int array[], int N, int item) {

    for (int i = 0; i < N; i++) {

        if (array[i] == item)
            return i;

    }

    return -1;
}