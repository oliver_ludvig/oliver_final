/** search.h
* ===========================================================
* Name: First Last
* Section: xxx
* Project: xxx
* Purpose: xxx
* ===========================================================
*/
#ifndef USAFA_CS220_S18_STUDENT_SEARCHLIBRARY_H
#define USAFA_CS220_S18_STUDENT_SEARCHLIBRARY_H

/** ----------------------------------------------------------
 * binSearch() searches array for a given int using binary search
 * @param array is the array of integers
 * @param lBound is the left index of the part of the
 * array to search
 * @param rBound is the right index of the part of the
 * array to search
 * @param item is the integer to find
 * @return returns -1 on not found or the index of the item
 * ----------------------------------------------------------
 */
int binSearch(int array[], int lBound, int rBound, int item);

/** ----------------------------------------------------------
 * linearSearch() finds the location of the first integer in the
 * given arry
 * @param array is the array of integers
 * @param N size of the array
 * @param item is the integer to find
 * @return location of int in array or -1 on error
 * ----------------------------------------------------------
 */
int linearSearch(int array[], int N, int item);

#endif //USAFA_CS220_S18_STUDENT_SEARCHLIBRARY_H
