/** TernaryTree.h
 * ===========================================================
 * Name: Ludvig Oliver, 4/4/2018
 * Section: T3/T4
 * Project: PEX3
 * Purpose: Create and manipulate a list of words.
 * ===========================================================
 */

#ifndef USAFA_CS220_S18_STUDENT_TERNARYTREE_H
#define USAFA_CS220_S18_STUDENT_TERNARYTREE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "wordList.h"

typedef struct ternaryTreeNode {
    char data;
    struct ternaryTreeNode* left;
    struct ternaryTreeNode* center;
    struct ternaryTreeNode* right;
} TernaryTreeNode;


typedef struct ternaryTreeType {
    TernaryTreeNode *root;
} TernaryTreeType;



TernaryTreeType *ternaryTreeCreate();
void ternaryTreeInsertBalanced(WordList *word, TernaryTreeType *tree, int lBound, int rBound);

// Required functions
void ternaryTreeDelete(TernaryTreeType *tree);
void ternaryTreeInsert(TernaryTreeType *tree, char *word);
WordList* ternaryTreeFindPattern(TernaryTreeType *tree, char *pattern);

// pex3 functions
void stripNewline(char *word);
WordList *openFile(char *file);
void printList(WordList *endList);
void printRecursive(char *word);
void printPreorder(TernaryTreeNode* nodePtr);
void printBalanced(WordList *word, int lBound, int rBound);

#endif //USAFA_CS220_S18_STUDENT_TERNARYTREE_H
