/** TernaryTree.c
 * ===========================================================
 * Name: Ludvig Oliver, 4/4/2018
 * Section: T3/T4
 * Project: PEX3
 * Purpose: Create and manipulate a list of words.
 * ===========================================================
 *
 * Documentation:  I received help from C3C Matthew Kuhn on my FindPattern Functionality.
 *                 He pretty much walked me through the whole process to make it work.  If
 *                 my code looks very similar to his it is because he informed me of what exact
 *                 steps to take from his knowledge, without my seeing of his code.  Although
 *                 I have grasped the idea of the FindPattern, I do not know how to implement
 *                 the wildcard method which he walked me through to accomplish. I also received
 *                 help from C3C Allison Raines helped me understand the requirement for the
 *                 insertBalance function in order to ensure that the insert occurs in a binary
 *                 pattern.
 *
 */

#include "TernaryTree.h"

//---------------------------------------------------------------------
// Helper functions
void deleteRecursive (TernaryTreeNode *rootNode);
void insertRecursive (TernaryTreeNode *tempNode, char *word);
TernaryTreeNode *createNode ();
WordList *findPatternRecursive(TernaryTreeNode *Node, char* pattern);

//---------------------------------------------------------------------


TernaryTreeType *ternaryTreeCreate() {
    TernaryTreeType *tree = (TernaryTreeType *) malloc(sizeof(TernaryTreeType));
    tree->root = NULL;
    return tree;
}

void ternaryTreeDelete(TernaryTreeType *tree) {
    TernaryTreeNode *tempNode = tree->root;
    deleteRecursive(tempNode);
}


// Inserts the Word
void ternaryTreeInsert(TernaryTreeType *tree, char *word) {

    if (tree->root == NULL) {
        TernaryTreeNode *newNode = createNode();
        tree->root = newNode;
        insertRecursive(tree->root, word);
    } else {
        insertRecursive(tree->root, word);
    }

}

void insertRecursive (TernaryTreeNode *tempNode, char *word) {

    if (tempNode->data == NULL) {
        tempNode->data = *word;
        if (*word == '\0') {
            return;
        }
            // Allocates Memory To Center Node Then Steps In
        else if (tempNode->center == NULL) {
            TernaryTreeNode *nextNode = createNode();
            tempNode->center = nextNode;
            insertRecursive(tempNode->center, word + 1);
        } else {
            // Steps into Center Node with new letter
            insertRecursive(tempNode->center, word + 1);
        }
    } else if (*word < tempNode->data) { // Less Than Move Left
        // If left node is NULL create node then step in
        if (tempNode->left == NULL) {
            TernaryTreeNode *nextNode = createNode();
            tempNode->left = nextNode;
            insertRecursive(tempNode->left, word);
        } else {
            insertRecursive(tempNode->left, word);
        }
    } else if (*word > tempNode->data) { // Greater Than Move Right
        // If right node is NULL create node then step in
        if (tempNode->right == NULL) {
            TernaryTreeNode *nextNode = createNode();
            tempNode->right = nextNode;
            insertRecursive(tempNode->right, word);
        } else {
            insertRecursive(tempNode->right, word);
        }
    } else {
        // If the Same Letter but center is NULL then create node then step in
        if (tempNode->center == NULL) {
            TernaryTreeNode *nextNode = createNode();
            tempNode->center = nextNode;
            insertRecursive(tempNode->center, word + 1);
        } else {
            insertRecursive(tempNode->center, word + 1);
        }
    }
}


void ternaryTreeInsertBalanced(WordList *word, TernaryTreeType *tree, int lBound, int rBound) {
    if (lBound < rBound) {
        int mid = (lBound + rBound) / 2;
        ternaryTreeInsert(tree, wordListGet(word, mid));
        ternaryTreeInsertBalanced(word, tree, lBound, mid); // left side
        ternaryTreeInsertBalanced(word, tree, mid + 1, rBound); // right side
    }
}

void deleteRecursive (TernaryTreeNode *rootNode) {
    if (rootNode != NULL) {
        deleteRecursive(rootNode->left);
        deleteRecursive(rootNode->right);
        deleteRecursive(rootNode->center);
        free(rootNode);
    }
}


TernaryTreeNode *createNode () {

    // Creates New Node
    TernaryTreeNode *newNode = (TernaryTreeNode *) malloc(sizeof(TernaryTreeNode));

    if (newNode == NULL) {
        printf("Error in createNode. No more memory!");
        exit(1);
    }

    // Creates New Node with NULL inputs
    newNode->data = NULL;
    newNode->left = NULL;
    newNode->right = NULL;
    newNode->center = NULL;

    return newNode;
}

WordList *ternaryTreeFindPattern(TernaryTreeType *tree, char *pattern) {

    if(tree->root == NULL) {
        printf("Tree is Empty.");
        exit(1);
    } else if (*pattern == NULL) {
        printf("No Pattern");
        exit(1);
    } else {
        WordList *endList = findPatternRecursive(tree->root,pattern);
        if (endList == NULL) {
            printf("Pattern Not In List");
            return endList;
        } else {
            return endList;
        }
    }
}


WordList *findPatternRecursive(TernaryTreeNode *Node, char* pattern) {

    WordList *foundList = wordListCreate();

    if (Node == NULL) {
        return foundList;
    }

    if (Node->data == *pattern) {
        if (*pattern == '\0') {
            foundList->numElements = -1;
            return foundList;
    } else {
            foundList = findPatternRecursive(Node->center, pattern + 1);
            char *prefix = calloc(2, sizeof(char));
            memcpy(prefix, Node, 1);
            if (foundList == NULL) {
                return foundList;
            }
            if (foundList->numElements == -1) {
                foundList->numElements = 0;
                wordListAdd(foundList, prefix);
            } else {
                wordListAppendPrefix(foundList, prefix);
            }
            free(prefix);
            return foundList;
        }
    }

    if (*pattern == '.') {

        WordList *leftList = findPatternRecursive(Node->left, pattern);

        WordList *centerList = findPatternRecursive(Node->center, pattern + 1);

        WordList *rightList = findPatternRecursive(Node->right, pattern);

        char *prefix = calloc(2, sizeof(char));
        memcpy(prefix, Node, 1);

        if (centerList->numElements == -1) {
            centerList->numElements = 0;
            wordListAdd(centerList, prefix);
        } else {
            wordListAppendPrefix(centerList, prefix);
        }

        wordListCombine(foundList, leftList);
        wordListCombine(foundList, centerList);
        wordListCombine(foundList, rightList);

        wordListDelete(leftList);
        wordListDelete(centerList);
        wordListDelete(rightList);

        return foundList;

    }

    if (*pattern < Node->data) {
        return findPatternRecursive(Node->left, pattern);
    } else {
        return findPatternRecursive(Node->right, pattern);
    }

}



void stripNewline(char *word) {
    char *newLine;
    if ((newLine = strchr(word, '\n')) != NULL) {
        *newLine = '\0';
    }
    if ((newLine = strchr(word, '\r')) != NULL) {
        *newLine = '\0';
    }
}


WordList *openFile(char *file) {
    // Opens File and Creates List of Words
    FILE *in = fopen(file, "r");
    if (in == NULL) {
        printf("Error opening data file: %s.\n", strerror(errno));
        exit(1);
    }
    WordList *List = wordListCreate();
    char word[MAXSTRINGLENGTH];
    while(fgets(word, MAXSTRINGLENGTH, in)) {
        stripNewline(word);
        wordListAdd(List, word);
    }
    fclose(in);
    return List;
}


void printList(WordList *endList) {

    // Initialize Required Numbers
    int listSize = wordListSize(endList);
    int count = 0;

    while (count < listSize) {
        // Recursive Function to print the list
        printRecursive(wordListGet(endList, count));
        // Adds comma and space if not the last word
        if (count != listSize - 1) {
            printf(", ");
        }
        count++;
    }

}


void printRecursive(char *word) {

    // Prints the word recursively and base case is NULL
    if (*word == '\0') {
        return;
    } else {
        printf("%c", *word);
        printRecursive(word + 1);
    }

}


void printPreorder(TernaryTreeNode* nodePtr) {

    if (nodePtr == NULL) {
        return;
    }
    printf("%c", nodePtr->data);
    printPreorder(nodePtr->left);
    printPreorder(nodePtr->center);
    printPreorder(nodePtr->right);

}


void printBalanced(WordList *word, int lBound, int rBound) {
    if (lBound < rBound) {
        int mid = (lBound + rBound) / 2;
        printf("%s\n", wordListGet(word,mid));
        printBalanced(word, lBound, mid); // left side
        printBalanced(word, mid + 1, rBound); // right side
    }
}