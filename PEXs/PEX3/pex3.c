/** pex3.c
 * ===========================================================
 * Name: Ludvig Oliver, 4/4/2018
 * Section: T3/T4
 * Project: PEX3
 * Purpose: Create and manipulate a list of words.
 * ===========================================================
 *
 * PEX3 - Scrabble
 *
 * Documentation: I received help from C3C Kishan Patel on loading
 *                my dictionary and informing of the utilization of the
 *                fgets function to add words to the list.
 *
 */


#include "wordList.h"
#include "TernaryTree.h"


int main() {

    // Creates List of the Dictionary
    WordList *dictionaryList = openFile("../PEXs/PEX3/test_dict1.txt");

    // Creates List of the Pattern
    WordList *patternList = openFile("../PEXs/PEX3/test1.txt");

    // Establishes Boundaries
    int lBound = 0;
    int rBound = wordListSize(dictionaryList);

    // Creates Ternary Tree
    TernaryTreeType *tree = ternaryTreeCreate();

    // Inputs List In Ternary Tree
    ternaryTreeInsertBalanced(dictionaryList, tree, lBound, rBound);

    int size = wordListSize(patternList);

    for (int i = 0; i < size; i++) {

        WordList *endList = ternaryTreeFindPattern(tree, wordListGet(patternList, i));

        if (endList != NULL) {
            printList(endList);
            printf("\n");
        }

    }

    // Delete Ternary Tree
    ternaryTreeDelete(tree);

    // Delete Lists
    wordListDelete(dictionaryList);
    wordListDelete(patternList);

    return 0;

};
