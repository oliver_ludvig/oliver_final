/** ternaryTreeUnitTest.c
 * ===========================================================
 * Name: Ludvig Oliver, 4/4/2018
 * Section: T3/T4
 * Project: PEX3
 * Purpose: Create and manipulate a list of words.
 * ===========================================================
 *
 * PEX3 - Scrabble Unit Test
 *
 * Documentation: Received no help on my unit test.
 *
 */

#include "wordList.h"
#include "TernaryTree.h"


int main() {

    // Balanced Tree Insertion

    // Test One
    printf("Balanced Test One\n");
    WordList *balancedOneList = openFile("../PEXs/PEX3/balancedOne.txt");
    printBalanced(balancedOneList, 0, wordListSize(balancedOneList));
    wordListDelete(balancedOneList);

    // Test Two
    printf("\nBalanced Test Two:\n");
    WordList *balancedTwoList = openFile("../PEXs/PEX3/balancedTwo.txt");
    printBalanced(balancedTwoList, 0, wordListSize(balancedTwoList));
    wordListDelete(balancedOneList);


    // Tree Insertion

    // Insert Test One
    printf("\nInsert Test One:\n");
    WordList *insertOneList = openFile("../PEXs/PEX3/insertOne.txt");
    TernaryTreeType *insertOneTree = ternaryTreeCreate();
    ternaryTreeInsertBalanced(insertOneList, insertOneTree, 0, wordListSize(insertOneList));
    wordListDelete(insertOneList);
    printPreorder(insertOneTree->root);
    printf("\n");

    // Insert Test Two
    printf("\nInsert Test Two:\n");
    WordList *insertTwoList = openFile("../PEXs/PEX3/insertTwo.txt");
    TernaryTreeType *insertTwoTree = ternaryTreeCreate();
    ternaryTreeInsertBalanced(insertTwoList, insertTwoTree, 0, wordListSize(insertTwoList));
    wordListDelete(insertTwoList);
    printPreorder(insertTwoTree->root);
    printf("\n");

    // Pattern Search

    // Pattern Test One
    printf("\nPattern Test One:\n");
    WordList *patternOne = openFile("../PEXs/PEX3/patternOne.txt");

    int sizeOne = wordListSize(patternOne);
    WordList *foundOneFinal = wordListCreate();

    for (int i = 0; i < sizeOne; i++) {
        WordList *foundOne = ternaryTreeFindPattern(insertOneTree, wordListGet(patternOne, i));
        if (foundOne != NULL) {
            wordListCombine(foundOneFinal, foundOne);
        }
        wordListDelete(foundOne);
    }
    printList(foundOneFinal);
    printf("\n");

    // Pattern Test Two
    printf("\nPattern Test Two:\n");
    WordList *patternTwo = openFile("../PEXs/PEX3/patternTwo.txt");

    int sizeTwo = wordListSize(patternTwo);
    WordList *foundTwoFinal = wordListCreate();

    for (int i = 0; i < sizeTwo; i++) {
        WordList *foundTwo = ternaryTreeFindPattern(insertTwoTree, wordListGet(patternTwo, i));
        if (foundTwo != NULL) {
            wordListCombine(foundTwoFinal, foundTwo);
        }
        wordListDelete(foundTwo);
    }
    printList(foundTwoFinal);
    printf("\n");

    // Frees all used memory
    wordListDelete(foundOneFinal);
    wordListDelete(foundTwoFinal);
    ternaryTreeDelete(insertOneTree);
    ternaryTreeDelete(insertTwoTree);

};


