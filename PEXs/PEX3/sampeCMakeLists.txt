cmake_minimum_required(VERSION 3.9)

# Set the title of your project and language
project(usafa_cs220_s18_student C)

# Set the compiler you will be using
set(CMAKE_C_STANDARD 11)

# Turn on helpful compiler warnings
set(CMAKE_C_FLAGS -Wall)
#set(CMAKE_EXE_LINKER_FLAGS "-Wl,--stack,900000000") #for memory allocation of large arrays

# Set the source directory and tell cmake what your
# source files are
set(SDIR PEXs/PEX3/)
set(SOURCE_FILES
        ${SDIR}wordList.h
        ${SDIR}wordList.c.o
        ${SDIR}ternaryTree.h
        ${SDIR}ternaryTree.c
        ${SDIR}pex3.c)

# Tell cmake the name of your executable and what source files to
# build from
add_executable(theExe ${SOURCE_FILES})
