/** pex0.c - Genome Sequencing
 * ===================================================================================================================
 * Name: C3C Ludvig Oliver, 23 Jan 2018
 * Section: M3A/M4A - LtCol Caswell
 * Project: Pex0.c - Genome Sequencing
 *          This assignment requires you to compare and measure differences between deoxyribonucleic acid (DNA)
 *          sequences.  Given 5 five function headers, implement the required parameters to accomplish the desired
 *          out come of each function.
 * Purpose: The purpose of this lab is to gain a better understanding of basic C functionality and the use of C
 *          standard library functions while understanding the relationship between C header files and C implementation
 *          files.  Also, it is to understand function calls and the power of code reuse.  This is accomplished through
 *          the exercise of basic comprehension of hamming distance and similiarity scores and their relationship
 *          to genome sequences.
 * Documentation: I received help from C3C Matthew Kuhn on approaching question 2.3 Count Matches.  He assisted me by
 *                talking me through the problem and affirming or correcting any ideas I had that would work on the
 *                problem.  During this conversation the function memcpy() came up and what its possible use is.
 *                This resulted to my adaptation of the function to my code in finding the best match.
 * ===================================================================================================================
 */

#include "pex0.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {

    // You may add code to test your functions above this comment, however be sure to not change
    // the test code below.  Please uncomment the code below when you are ready to test your PEX.
    // Once you are satisfied that you've got it working correctly comment out any code you've added
    // above and leave the test code below uncommented.

    printf("hamming = %d\n", hammingDistance("AAA", "CC")); // result= -1
    printf("hamming = %d\n", hammingDistance("ACCT", "ACCG")); //result= 1
    printf("hamming = %d\n", hammingDistance("ACGGT", "CCGTT")); //result= 2

    printf("simularity = %0.3f\n", similarityScore("CCC", "CCC")); // result= 1.0
    printf("simularity = %0.3f\n", similarityScore("ACCT", "ACCG")); // result= 0.75
    printf("simularity = %0.3f\n", similarityScore("ACGGT", "CCGTT")); // result= 0.6
    printf("simularity = %0.3f\n", similarityScore("CCGCCGCCGA", "CCTCCTCCTA")); // result= 0.7

    printf("# matches = %d\n", countMatches("CCGCCGCCGA", "TTT", 0.6)); // result= 0
    printf("# matches = %d\n", countMatches("CCGCCGCCGA", "CCG", 0.2)); // result= 8
    printf("# matches = %d\n", countMatches(MOUSEDNA, "CGCTT", 0.5)); // result= 36
    printf("# matches = %d\n", countMatches(HUMANDNA, "CGC", 0.3)); // result= 215

    printf("best match = %0.3f\n", findBestMatch("CCGCCGCCGA", "TTT")); // result= 0.0
    printf("best match = %0.3f\n", findBestMatch("CTGCCACCAA", "CCGC")); // result= 0.75
    printf("best match = %0.3f\n", findBestMatch(MOUSEDNA, "CGCTT")); // result= 0.8
    printf("best match = %0.3f\n", findBestMatch(HUMANDNA, "CGC")); // result= 1.0

    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAGGCCGGTT")); //result= 1
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAGGCCGGG")); //result= 2
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTAATTCTTTT")); //result= 3
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAG")); //result= 0

    return 0;
}

/** ------------------------------------------------------------
 *  Hamming Distance - A common way to measure the similarity
 *  between two strings of the SAME LENGTH. Compares the two
 *  strings position by position and counting the strings that
 *  are different
 * @param str1: First string to be compared
 * @param str2: Second string to be compared
 * @return: int: number of strings that are different
 *          (-1 if the strings are not the same size)
 */
int hammingDistance(char *str1, char *str2) {
    int count = 0; // Initializes Count

    if (strlen(str1) != strlen(str2))  // Compares if two strings are the same length
        return -1;  // Returns -1 if strings are not the same length
    else
        for (int i = 0; i < strlen(str1); i++) {
            if (str1[i] != str2[i])  // Compares each character of string side by side
                count += 1;  // Adds to the count if the characters are different
        }

    return count;  // Returns the number of differences found between the two strings
}

/** ------------------------------------------------------------
 * Similarity Score - Calculates the similarity score between
 * two strings of the same length (Uses hamming distance to find
 * differences between the strings)
 * Similarity = (SequenceLength - HammingDistance) / SequenceLength
 * @param seq1: First string to be compared
 * @param seq2: Second string to be compared
 * @return: float: the similarities between the two strings
 */

float similarityScore(char *seq1, char *seq2) {

    float similarity;  // Initializes the return value
    float length = strlen(seq1);  // Finds the length of the sequences
    float hamming = hammingDistance(seq1, seq2);  // Calls the hamming function to find differences

    similarity = (length - hamming) / length;  // Calculates the similarity to be returned

    return similarity;  // Returns the similarity score of the two strings
}

/** ------------------------------------------------------------
 * Count Matches - Count the number of matches a given sequence
 * has in a given genome. Examines all positions of the genome for
 * an occurrence of the sequence and identifies how many matches
 * are found above a given minimum similarity score.
 * @param genome: Genome to be compared by the sequence
 * @param seq: Sequence to find matches/similarities in the genome
 * @param minScore:  Minimum score to be counted in matches
 * @return int: Number of matches/similarities found above a given
 *              minimum similarity score
 */
int countMatches(char *genome, char *seq, float minScore) {
    int count = 0;  // Initializes the counter

    for (int i = 0; i < strlen(genome); i++) {
        float calc = 0;  // Initializes a calculator to be used for comparison to the minimum score
        char temp[100] = "";  // Initializes a temporary string to fill with genome sequence

        memcpy(temp, genome + i, strlen(seq));  // Copies positions of the genome within sequence length

        for (int x = 0; x < strlen(seq); x++) {  // Iterates through the temp and sequence strings
            if (temp[x] == seq[x] && strlen(temp) == strlen(seq))  // Compares the temp and sequence by character
                calc += 1;  // Adds to the calculator if characters are the same
        }

        if (calc / strlen(seq) > minScore)  // Compares the calculated matches and the minimum score
            count += 1;  // Adds to the count if the calculated matches is larger than the minimum score
    }

    return count;  // Returns number of matches/similarities above a given minimum score
}

/** ------------------------------------------------------------
 * Best Match - Determines the highest similarity score at any
 * position in the genome given a sequence to match.  Checks every
 * position using similarityScore().
 * @param genome: Genome to be scanned for the highest similarity score
 * @param seq: The sequence to be matched
 * @return float: The best match at any given point in the genome
 */
float findBestMatch(char *genome, char *seq) {

    float best = 0;  // Initializes best number to be returned
    float score = 0;

    for (int i = 0; i < strlen(genome); i++) {  // Iterates through the genome to find the best match at any position
        char temp[100] = "";  // Initializes a temporary string to fill with genome sequence

        memcpy(temp, genome + i, strlen(seq));  // Copies positions of the genome within sequence length

        if (strlen(temp) == strlen(seq)) // Only takes into account sequences of the same length
            score = similarityScore(temp, seq);  // Uses similarityScore() to find best match

        if (score > best)
            best = score;  // Checks to see which score is the best and

    }

    return best;  // Returns the best match
}

/** ------------------------------------------------------------
 * Best Genome - Finds the best matching genome between 3 known
 * genome samples given an unknown sequence through the use of
 * findBestMatch().
 * @param genome1: First genome sample to be compared
 * @param genome2: Second genome sample to be compared
 * @param genome3: Third genome sample to be compared
 * @param unkownSeq: The unknown sequence to be used for matching
 * @return int: The number of the genome that has the best match
 */
int findBestGenome(char *genome1, char *genome2, char *genome3, char *unkownSeq) {
    int best = 0;  // Defaults to 0 if the largest value has any ties

    float case1 = findBestMatch(genome1, unkownSeq);  // Finds the best match for genome 1
    float case2 = findBestMatch(genome2, unkownSeq);  // Finds the best match for genome 2
    float case3 = findBestMatch(genome3, unkownSeq);  // Finds the best match for genome 3

    if (case1 > case2) {  // Compares if genome 1 is better than genome 2
        if (case1 > case3)  // Also compares if genome 1 is better than genome 3
            best = 1;  // If so genome 1 is the best
    }
    if (case2 > case3) {  // Compares if genome 2 is better than genome 3
        if (case2 > case1)  // Also compares if genome 2 is better than genome 1
            best = 2;  // If so genome 2 is the best
    }
    if (case3 > case1) {  // Compares if genome 3 is better than genome 1
        if (case3 > case2)  // Also compares if genome 3 is better than genome 2
            best = 3;  // If so genome 3 is the best
    }

    return best;  // Returns the best genome
}
