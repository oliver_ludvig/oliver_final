/** pex0.h - Genome Sequencing
 * ===================================================================================================================
 * Name: C3C Ludvig Oliver, 23 Jan 2018
 * Section: M3A/M4A - LtCol Caswell
 * Project: Pex0.h - Genome Sequencing
 *          This assignment requires you to compare and measure differences between deoxyribonucleic acid (DNA)
 *          sequences.  Given 5 five function headers, implement the required parameters to accomplish the desired
 *          out come of each function.
 * Purpose: The purpose of this lab is to gain a better understanding of basic C functionality and the use of C
 *          standard library functions while understanding the relationship between C header files and C implementation
 *          files.  Also, it is to understand function calls and the power of code reuse.  This is accomplished through
 *          the exercise of basic comprehension of hamming distance and similiarity scores and their relationship
 *          to genome sequences.
 * ===================================================================================================================
 */

#ifndef USAFA_CS220_S18_STUDENT_PEX0_H
#define USAFA_CS220_S18_STUDENT_PEX0_H

#define HUMANDNA "CGCAAATTTGCCGGATTTCCTTTGCTGTTCCTGCATGTAGTTTAAACGAGATTGCCAGCACCGGGTATCATTCACCATTTTTCTTTTCGTTAACTTGCCGTCAGCCTTTTCTTTGACCTCTTCTTTCTGTTCATGTGTATTTGCTGTCTCTTAGCCCAGACTTCCCGTGTCCTTTCCACCGGGCCTTTGAGAGGTCACAGGGTCTTGATGCTGTGGTCTTCATCTGCAGGTGTCTGACTTCCAGCAACTGCTGGCCTGTGCCAGGGTGCAGCTGAGCACTGGAGTGGAGTTTTCCTGTGGAGAGGAGCCATGCCTAGAGTGGGATGGGCCATTGTTCATG"
#define MOUSEDNA "CGCAATTTTTACTTAATTCTTTTTCTTTTAATTCATATATTTTTAATATGTTTACTATTAATGGTTATCATTCACCATTTAACTATTTGTTATTTTGACGTCATTTTTTTCTATTTCCTCTTTTTTCAATTCATGTTTATTTTCTGTATTTTTGTTAAGTTTTCACAAGTCTAATATAATTGTCCTTTGAGAGGTTATTTGGTCTATATTTTTTTTTCTTCATCTGTATTTTTATGATTTCATTTAATTGATTTTCATTGACAGGGTTCTGCTGTGTTCTGGATTGTATTTTTCTTGTGGAGAGGAACTATTTCTTGAGTGGGATGTACCTTTGTTCTTG"
#define CATDNA "CGCATTTTTGCCGGTTTTCCTTTGCTGTTTATTCATTTATTTTAAACGATATTTATATCATCGGGTTTCATTCACTATTTTTCTTTTCGATAAATTTTTGTCAGCATTTTCTTTTACCTCTTCTTTCTGTTTATGTTAATTTTCTGTTTCTTAACCCAGTCTTCTCGATTCTTATCTACCGGACCTATTATAGGTCACAGGGTCTTGATGCTTTGGTTTTCATCTGCAAGAGTCTGACTTCCTGCTAATGCTGTTCTGTGTCAGGGTGCATCTGAGCACTGATGTGGAGTTTTCTTGTGGATATGAGCCATTCATAGTGTGGGATGTGCCATAGTTCATG"

int hammingDistance(char *str1, char *str2);

float similarityScore(char *seq1, char *seq2);

int countMatches(char *genome, char *seq, float minScore);

float findBestMatch(char *genome, char *seq);

int findBestGenome(char *genome1, char *genome2, char *genome3, char *unkownSeq);

#endif //USAFA_CS220_S18_STUDENT_PEX0_H
