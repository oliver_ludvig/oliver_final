/** example10a.c
* ===========================================================
* Name: Troy Weingart
* Section: n/a
* Project: Lsn12 - Pass by Reference Example
* ===========================================================
*/
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#define MAX_VALUES 12
#define MAX_STUDENTS 100

typedef struct {
    char name[50];
    int age;
    int squad;
    int classYear;
    int numGrades;
    int grades[MAX_VALUES];
} MyCadetInfo;

// Local function prototypes
int getData(MyCadetInfo cadetRecords[]);
void adjustGrade(MyCadetInfo* cadetRecords, int gradeNum, int points);

int main() {

    // declare an array of student records
    MyCadetInfo myStudents[MAX_STUDENTS];

    // read in cadet information
    getData(myStudents);

    // add 5 pts to the 3rd grade for the ith cadet
    adjustGrade(&myStudents[1],3,5);

    return 0;
}

/** adjustGrade()- modifies a given cadet record with an adjusted score on
 * a given assignmet
 * @param cadetRecord - a pointer to a cadet record
 * @param gradNum - which grade to modify
 * @param ponts - how many points to adjust by
 */
void adjustGrade(MyCadetInfo* cadetRecord,int gradeNum, int points) {

    // the below two statements are equivalent, the use of the -> operator
    // is much more common, however there are instances where you will
    // need to use the (*pointer) notation
    cadetRecord->grades[gradeNum] += points;
    //(*cadetRecord).grades[gradeNum] += points;
}


/** ----------------------------------------------------------
 * getDataText() is used to read MyCadetInfo typed records
 * from a file
 * @param cadetRecords is the array of cadet records
 * @return number of records read
 * ----------------------------------------------------------
 */
int getData(MyCadetInfo cadetRecords[]) {

    // Open an input file for reading
    FILE *in = fopen("../Lessons/lsn12/lsn12Data.txt", "r");
    if (in == NULL) {
        printf("Error opening data file: %s.\n", strerror(errno));
        exit(1);
    }

    char firstName[30];
    char lastName[45];
    int numRead = 0;

    while (numRead < MAX_STUDENTS && !feof(in)) {
        fscanf(in, "%s %s %d %d %d", firstName, lastName, &cadetRecords[numRead].age,
               &cadetRecords[numRead].squad, &cadetRecords[numRead].classYear);
        strcat(firstName, " ");
        strcpy(cadetRecords[numRead].name, strcat(firstName, lastName));
        cadetRecords[numRead].numGrades = 4;
        for (int i=0; i<cadetRecords[numRead].numGrades; i++) {
            cadetRecords[numRead].grades[i] = random() % 101;
        }
        numRead++;
    }
    fclose(in);
    return numRead;
}