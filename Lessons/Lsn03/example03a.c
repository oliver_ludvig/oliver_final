/** example03a.c
* ===========================================================
* Name: Troy Weingart, 4 Jan 18
* Section: n/a
* Project: Lesson 3 examples
* Purpose: Demonstrate headers and recursion in C
* ===========================================================
*/
#include "example03a.h"
#include <stdio.h>

int main() {
    // Call the two versions of sumOfInts
    printf("Sum of ints to 10 is %d.\n", sumOfInts(10));
    printf("Sum of ints to 10 is %d.\n", sumOfIntsRec(10));

    // Call the two versions of factorial
    printf("10 factorial is %d.\n", factorial(10));
    printf("10 factorial is %d.\n", factorialRec(10));

    // Call numberOfRec()
    int array[] = {10, 23, 23, 1, 3, 10, 5, 1, 7, 10};
    int sizeOfArray = sizeof(array) / sizeof(array[0]);
    printf("The number of 10's in the array is %d.\n", numberOfRec(array, sizeOfArray - 1, 10));

    return 0;
}

/** ----------------------------------------------------------
 * sumOfInts() sumOfIntsRec() calcuate the sum of integers
 * from 1 to the number passed into the function
 * @param num is the upper bound on the sub
 * @return sum
 * ----------------------------------------------------------
 */
int sumOfInts(int num) {
    int sum = 0;
    for (int i = 1; i <= num; i++) {
        sum += i;
    }
    return sum;
}

int sumOfIntsRec(int num) {
    // the base case - stops recursion typically
    // a simplified version of the larger problem

    // the recursive case - the function
    // calls itself on a smaller
    // version of the large problem

    if (num == 0)
        return 0;
    else
        return num + sumOfInts(num - 1);
}

/** ----------------------------------------------------------
 * factorial() computes the factorial of passed number
 * @param num is the operand of the factorial operation
 * @return num!
 * ----------------------------------------------------------
 */
int factorial(int num) {
    int fact = 1;
    for (int i = 1; i <= num; i++) {
        fact *= i;
    }
    return fact;
}


int factorialRec(int num) {
    if (num == 1)
        return 1;
    else
        return num * factorialRec(num - 1);
}

/** ----------------------------------------------------------
 * numberOfRec() counts the number of time a given number
 * appears in an array of integers
 * @param array is the array of integers
 * @param index is the starting index (typically the end)
 * @param target is the integer to be counted
 * @return the number of target integers in the array
 * ----------------------------------------------------------
 */
int numberOfRec(int array[], int index, int target) {
    if (index < 0)
        return 0;
    if (array[index] == target)
        return 1 + numberOfRec(array, index - 1, target);
    return numberOfRec(array, index - 1, target);
}