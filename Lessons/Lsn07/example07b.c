/** example07a.c
 * ===========================================================
 * Name: Troy Weingart
 * Section: n/a
 * Project: Lesson 7 - Insertion into an array in C
 * ===========================================================
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define n 5

int insert(int array[], int curSize, int maxSize, int value, int pos);

int main() {

    //set up for and then seed random number
    //generator
    struct timespec time;
    clock_gettime(CLOCK_REALTIME,&time);
    srandom((unsigned) (time.tv_nsec ^ time.tv_sec));

    int curSize = 0;
    int maxSize = 5 * n / 3; //allow for growth
    int nums[maxSize];

    // fill array with random ints from 0 to 9999
    for (int i = 0; i < n; i++) {
        nums[i] = (int) random() % 10000;
        curSize++;
        printf("%d\n", nums[i]);
    }

    curSize = insert(nums, curSize, maxSize, -1, curSize / 2); // insert in middle
    curSize = insert(nums, curSize, maxSize, -1, 0);  // insert at front
    curSize = insert(nums, curSize, maxSize, -1, maxSize-1);  // insert at end

    return 0;
}

int insert(int array[], int curSize, int maxSize, int value, int pos) {
    if (curSize == maxSize) {
        return -1; // error full
    }
    //handle special case for insertion at the end of the array
    if (pos == curSize - 1) {
        array[pos] = value;
        return ++curSize; //odd what does this do? vs curSize++
    }

    //beginning and middle case is the same
    //need to shift first from pos to end of
    //the array to make room
    for (int i = curSize - 1; i >= pos; i--) {
        array[i+1] = array[i];
    }
    array[pos] = value;
    return ++curSize;
}