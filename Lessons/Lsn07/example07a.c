/** example07a.c
 * ===========================================================
 * Name: Troy Weingart
 * Section: n/a
 * Project: Lesson 7 - Random Numbers in C
 * ===========================================================
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define n 5

//int main() {
//
//    int nums[n];
//
//    // fill array with random ints from 0 to 9999
//    for (int i = 0; i < n; i++) {
//        nums[i] = rand() % 10000;
//        printf("%d\n", nums[i]);
//    }
//    return 0;
//}

int main() {
    //lets "properly" generate a sequence of random numbers by
    //first seeding the random generator with something that is
    //variable, unlike the default but how to get a unique number
    //every time we run it

    struct timespec time1,time2;
    time_t time3, time4;

    clock_gettime(CLOCK_REALTIME,&time1);
    clock_gettime(CLOCK_REALTIME,&time2);
    time3 = time(NULL);
    time4 = time(NULL);
    printf("t1, secs %d, nsecs %ld\n",(int) time1.tv_sec,time1.tv_nsec);
    printf("t2, secs %d, nsecs %ld\n",(int) time2.tv_sec,time2.tv_nsec);
    printf("t3, secs %d\n",(int) time3);
    printf("t4, secs %d\n",(int) time4);

    //srandom(1); // default seed is 1 to random() if not specified with call to srandom()
    srandom(time1.tv_nsec ^ time1.tv_sec); // much better way to seed with nano second resolution
                              // for multithreaded applications,

    int nums[n];

    // fill array with random ints from 0 to 9999
    for (int i = 0; i < n; i++) {
        nums[i] = random() % 10000;
        printf("%d\n", nums[i]);
    }
    return 0;
}
