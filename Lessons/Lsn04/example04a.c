/** example04a.c
 * ===========================================================
 * Name: Troy Weingart, 4 Jan 18
 * Section: n/a
 * Project: Lesson 4 string.h example
 * ===========================================================
*/
#include "example04a.h"
#include <stdio.h>
#include <string.h>

int main() {
    // declare a couple of strings
    char str0[] = {"xxxxxx\0"};
    char str1[] = {"12345\0"};
    char* str2 = {"123456789\0"};

    // print with %s operator, the NULL terminator is not printed
    printf("->%s<-\n",str1);
    printf("->%s<-\n",str2);

    // strlen() gives you the length of a string, the NULL terminator
    // is not counted
    printf("len str1 = %lu\n", strlen(str1));
    printf("len str1 = %lu\n", strlen(str2));

    // because string are arrays of char we can print single characters
    // using array indexing notation
    printf("char %d = %c, of ->%s<-\n",0,str1[0],str1);
    printf("char %d = %c, of ->%s<-\n",3,str2[3],str2);

    // we can print the NULL terminator for fun...notice its a wierd box or
    // a 0 when printed as an integer
    printf("char %lu = %c, of ->%s<-\n",strlen(str1),str1[strlen(str1)],str1);
    printf("char %lu = %d, of ->%s<-\n",strlen(str2),str2[strlen(str2)],str2);

    // we can change individual characters also...or can we?
    str1[3] = '*';

    printf("->%s<-\n",str1);
   // str2[3] = '*'; // there is something amiss here (debugger?)
    printf("->%s<-\n",str2);

    // we have issues if we do not have a large enough string to perform
    // some operations...always be aware of how strings are terminated
    // and how long they are and how long the resulting string is when the
    // operation completes

    //strcat(str0,"*************"); //destination is too small, fix?
    strcpy(str0,"**"); // works but what happens to remainder of str0
    strncpy(str0,"**",2); // works notie remainder
    printf("->%s<-\n",str0);
    printf("->%s<-\n",str1);

    // what about strcmp()?
    printf("strcmp(\"aaa\",\"aaa\") return val = %d\n",strcmp("aaa","aaa"));
    printf("strcmp(\"aaa\",\"ccc\") return val = %d\n",strcmp("aaa","ccc"));
    printf("strcmp(\"ccc\",\"aaa\") return val = %d\n",strcmp("ccc","aaa"));

    // how about passing a string to a function, an array of chars is just an array
    // so all techniques for arrays apply
    char theString[100]={"This is a test of the emergency broadcast system!!!\0"};
    printf("The number of t/T's is %d.\n",countT(theString));

    return 0;
}

/** ----------------------------------------------------------
 * countT() - counts the occurance of t or T in a string
 * @param str the sting to examing
 * @return number of t or T characters
 * ----------------------------------------------------------
 */
int countT(char str[]) {
    int count = 0;
    for (int i = 0 ; i < strlen(str); i++) {
        if (str[i] == 't' || str[i] == 'T') {
            count++;
        }
    }
    return count;
}