/** example03a.h
 * ===========================================================
 * Name: Troy Weingart, 4 Jan 18
 * Section: n/a
 * Project: Lesson 4 string.h example
 * ===========================================================
 */
#ifndef USAFA_CS220_S18_STUDENT_EX4A_H
#define USAFA_CS220_S18_STUDENT_EX4A_H

/** ----------------------------------------------------------
 * countT() - counts the occurance of t or T in a string
 * @param str the sting to examing
 * @return number of t or T characters
 * ----------------------------------------------------------
 */
int countT(char str[]);
#endif //USAFA_CS220_S18_STUDENT_EX4A_H
