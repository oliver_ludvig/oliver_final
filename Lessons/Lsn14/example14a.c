/** example14a.c
* ===========================================================
* Name: David Caswell
* Section: n/a
* Project: Lsn14 - Ptrs in C
* ===========================================================
*/
#include <stdio.h>
#include <stdlib.h>

struct test {
    int a;
    float b;
};

int main()
{
    int daVal = 10;
    printf("daVal == %d\n", daVal++);
    printf("daVal == %d\n", daVal);
    printf("daVal == %d\n", ++daVal);
    printf("daVal == %d\n,", daVal);

    int a = 4;
    int b = 6;
    int* ptrA = &a;
    int* ptrB = &b;

    printf("a: %d, address of a: %lu\n",a,ptrA);
    printf("b: %d, address of b: %lu\n",b,ptrB);
    printf("sizeof(int*) = %d\n", sizeof(int*));
    printf("sizeof(int) = %d\n",sizeof(int));

    // Different ways of accessing the address
    printf("address of value %lu or %lu\n", &a, ptrA);
    printf("dereference a pointer: %d\n\n", *ptrA);

    // pointers can point/change same address space
    int* ptrC; // dangling pointer since it is not initialized
    ptrC = ptrA; // C is pointing at same address as a
    printf("Addresses are the same %lu == %lu\n", ptrA, ptrC);
    *ptrC = 12;
    printf("Can change value at same memory because ptrC==ptrA changing *ptrC changes *ptrA: %d\n\n", *ptrA);

    int array[] = {20, 30, 40, 50, 60};

    // Array is an address
    int* arrPtr = array;
    printf("Array addresses: %lu or %lu\n\n", arrPtr, array);

    // You can change the address of a pointer
    printf("array[0] @ %lu, array @ %lu array[4] @ %lu with value %d\n\n", &array[0], array, arrPtr + 4,
           *(arrPtr+4));
    arrPtr ++;
    printf("element one: %d\n", *arrPtr);
    //ERROR Cannot do: array++ as "array" is a constant and cannot be modified

    // Ways to dereference an array
    printf("%d or %d or %d\n\n", *array, *(array + 0), array[0]);

    // Ptr to a structure
    struct test tempData;
    tempData.a = 15;
    tempData.b = 3.14;

    struct test* tempDataPtr;
    tempDataPtr = &tempData;

    printf("Data in temp using pointer to member operator: %f\n", tempDataPtr->b);
    printf("Data in temp using dereference: %f\n\n", (*tempDataPtr).b);

    // At this time int a and b from before are still accessible since they were originally declared
    *ptrA = 15; // sets value at the memory pointed to by A to 15
    *ptrB = 30;
    ptrC = NULL;
    printf("address of c: %lu\n",ptrC);

    printf("a: %d, address a: %p\n",a,ptrA);
    printf("b: %d, address b: %p\n\n",b,ptrB);

    // what would happen on the below
    ptrA = ptrB;
    printf("a: %d, address a: %p\n",a,ptrA);
    printf("b: %d, address b: %p\n",b,ptrB);
    printf("Value of a: %d\n\n", *ptrA);
    // Can we get back to original address?

    printf("value of *ptrC: %d\n",*ptrC);
}

