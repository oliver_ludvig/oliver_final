/** example25aHeap.h
 * ===========================================================
 * Name: Dr. Troy Weingart
 * Section:
 * Purpose: Implement a heap ADT and heap sort
 * ===========================================================
 */

#ifndef USAFA_CS220_S18_STUDENT_EXAMPLE25AHEAP_H
#define USAFA_CS220_S18_STUDENT_EXAMPLE25AHEAP_H
#include <stdbool.h>
#define MAX_HEAP_SIZE 6 // Maximum number of elements in heap plus 1

typedef struct {
    int numElem;   // Number of elements
    int array[MAX_HEAP_SIZE];  // Array to store heap
} Heap;

Heap* maxHeapCreate();
void insertElemHeap(Heap* heap, int element);
int removeElemHeap(Heap* heap);
void displayHeap(Heap* heap);
void sortHeap(Heap *heap);

#endif //USAFA_CS220_S18_STUDENT_EXAMPLE25AHEAP_H
