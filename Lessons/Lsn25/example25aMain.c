/** example25aMain.c
 * ===========================================================
 * Name: Troy Weingart
 * Section: n/a
 * Project: Example 25a Main
 * Purpose: Test the implementation of a max-heap / heap sort
 * ===========================================================
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "example25aHeap.h"

int main() {
    // Seed the random number generator
    // only needed if you are using random
    // numbers in insertElemHeap() call below
    struct timespec time;
    clock_gettime(CLOCK_REALTIME, &time);
    srandom((unsigned) (time.tv_nsec ^ time.tv_sec));

    // Static array for testing purposes
    int nums[] = {10, 30, 20, 55, 25};

    // create a new max heap
    Heap* myHeap = maxHeapCreate();

    // insert numbers into the heap on at a time
    for (int i = 0; i < (MAX_HEAP_SIZE-1); i++) {
        insertElemHeap(myHeap,nums[i]);
        // comment out line above and uncomment line below to use random numbers
        // insertElemHeap(myHeap,(int) (random() % 1000 + 1000));
    }
    displayHeap(myHeap);

    //remove items from heap one at a time and print them
    for (int i = 0; i < (MAX_HEAP_SIZE-1); i++) {
        printf("%d\n",removeElemHeap(myHeap));
    }
    printf("heap size -> %d\n", myHeap->numElem);
}