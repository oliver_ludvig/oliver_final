/** example01a.c
* ===========================================================
* Name: Troy Weingart, 21 Dec 17
* Section: n/a
* Project: Lesson 1 examples
* Purpose: Demonstrate basic c
* ===========================================================
*/
#include <stdio.h>

// function prototype, more on this later, but for now know
// that in c something has to be declared or specified prior
// to use
void printBits(size_t const size, void const *const ptr);

int main() {
    // Variable declarations, must be done prior to use
    int upperBound = 27;
    int lowerBound = 0;

    // C allows us to change how output is interpreted
    // with format specifiers
    printf("UpperBound = %d\n", upperBound);
    printf("UpperBound = %f\n", upperBound);
    printf("UpperBound = %x\n", upperBound);
    printf("UpperBound = %o\n", upperBound);
    printf("UpperBound = ");
    // There isn't a way to print in binary in the standard c library
    // so we wrote a helper function to accomplish this
    printBits(sizeof(upperBound), &upperBound);

    // IO from the console / keyboard
    printf("Enter lowerBound:");
    scanf("%d",&lowerBound);

    // using upper and lower bound print even
    // integers between [lower upper] bounds
    for (int i = lowerBound; i < upperBound; i++) {
        if (i % 2 == 0) {
            printf("%d ",i);
        }
    }
    printf("\n");

    return 0;
}

void printBits(size_t const size, void const *const ptr) {
    unsigned char *b = (unsigned char *) ptr;
    unsigned char byte;
    int i, j;

    for (i = size - 1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}