/** example05c.c
 * ===========================================================
 * Name: Troy Weingart
 * Section: n/a
 * Project: Lesson 5 Big-O example
 * ===========================================================
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {

    // Example #1 find f(n) & Big-O
    int n = atoi(argv[1]);
    int nums[n];
//    int count = 0;

    // find the largest value in the array
    int largest = 0;

    for (int i = 0; i < n; i++) {
        if (nums[i] > largest) {
            largest = nums[i];
        }
//        count+=4;
    }
//    printf("%d\t%d\n",n,count);
    return 0;
}