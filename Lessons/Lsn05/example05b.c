/** example05b.c
 * ===========================================================
 * Name: Troy Weingart
 * Section: n/a
 * Project: Lesson 5 struct example
 * ===========================================================
*/
#define SIZE 100
#include <string.h>

typedef struct {
//    char name[50];
    int age;
    int squad;
    int classYear;
} MyCadetInfo;

int main() {
    // declare a array of structs
    MyCadetInfo theInfoStructArray[SIZE];

    // exchange two of the records a few times
    for (int i = 0; i < 100000000; i++) {

        MyCadetInfo tempCadet = theInfoStructArray[99];
        theInfoStructArray[99] = theInfoStructArray[0];
        theInfoStructArray[0] = tempCadet;
    }

    return 0;
}