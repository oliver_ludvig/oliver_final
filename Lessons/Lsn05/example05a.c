/** example05a.c
 * ===========================================================
 * Name: Troy Weingart
 * Section: n/a
 * Project: Lesson 5 non struct example
 * ===========================================================
*/
#define SIZE 100
#include <string.h>

int main() {
    char name[SIZE][50];
    int age[SIZE];
    int squad[SIZE];
    int classYear[SIZE];

    // exchange two of the records a few times
    for (int i = 0; i < 100000000; i++) {

        char tempName[50];
        strcpy(tempName, name[99]);
        strcpy(name[99], name[0]);
        strcpy(name[0], tempName);

        int temp = age[99];
        age[99] = age[0];
        age[0] = temp;

        temp = squad[99];
        squad[99] = squad[0];
        squad[0] = temp;

        temp = classYear[99];
        classYear[99] = classYear[0];
        classYear[0] = temp;
    }
    return 0;
}