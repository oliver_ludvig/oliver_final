/** example14a.c
* ===========================================================
* Name: Troy Weingart
* Section: n/a
* Project: Lsn15 - Memory in C
* ===========================================================
*/
#include <stdlib.h>
#include <stdio.h>

void function1();
void function2();

char* alpha = "This is an example of unchangeable memory";
char beta[100] = "This global string can be changed";
char* gname = "DFCS";

int main() {
    int   gamma[10] = {10, 20, 30};  // stored on the run-time stack
    char* name = "DFCS";

    printf("Address of global variables\n");
    printf("Address of alpha = %lu\n", (unsigned long) alpha);
    printf("Address of beta  = %lu\n", (unsigned long) beta);

    printf("Address of local main variables\n");
    printf("Address of gamma = %lu\n", (unsigned long) gamma);
    printf("Address of name  = %lu\n", (unsigned long) name);
    printf("Address of gname = %lu\n", (unsigned long) gname);

    function1();
    function2();

    int* delta = malloc(100);
    printf("Address of dynamically allocated memory\n");
    printf("Address of delta = %lu\n", (unsigned long) delta);

    int* echo = malloc(100);
    printf("Address of echo = %lu\n", (unsigned long) echo);
}

void function1() {
    int    a;        // stored on the run-time stack
    float  b;        // stored on the run-time stack
    int    c[10];    // stored on the run-time stack
    char   d;        // stored on the run-time stack
    char * name = "DFCS";

    printf("Address of local variables in function1\n");
    printf("Address of a    = %lu\n", (unsigned long) &a);
    printf("Address of b    = %lu\n", (unsigned long) &b);
    printf("Address of c    = %lu\n", (unsigned long) c);
    printf("Address of d    = %lu\n", (unsigned long) &d);
    printf("Address of name = %lu\n", (unsigned long) name);
}

void function2() {
    int    x;
    float  y;
    int    z[10];
    char   w;
    char * name = "DFCS";

    printf("Address of local variables in function2\n");
    printf("Address of x    = %lu\n", (unsigned long) &x);
    printf("Address of y    = %lu\n", (unsigned long) &y);
    printf("Address of z    = %lu\n", (unsigned long) z);
    printf("Address of w    = %lu\n", (unsigned long) &w);
    printf("Address of name = %lu\n", (unsigned long) name);
}