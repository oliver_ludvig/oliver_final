/** example14a.c
* ===========================================================
* Name: Troy Weingart
* Section: n/a
* Project: Lsn15 - Dynamic Memory Allocation in C
* ===========================================================
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/errno.h>

#define MAX_VALUES 10

typedef struct {
    char name[50];
    int age;
    int squad;
    int classYear;
    int numGrades;
    int grades[MAX_VALUES];
} MyCadetInfo;

int getData(MyCadetInfo* cadetRecsPtr, int numRecs);

int main() {

    int numRecs;

    // ask the user how many cadet records there are
    printf("Enter the number of records: ");
    scanf("%d",&numRecs);

    // use malloc to allocate an array of cadet records
    // malloc returns NULL on success and NULL on failure
    // the only parameter is the number of Bytes to allocate
    // you will see below that I've used the "number of things *
    // size of thing" as a general way to dynamically allocate an
    // array of structs
    MyCadetInfo* cadetRecsPtr = malloc(numRecs * sizeof(MyCadetInfo));

    // We can now get an exact number records from the data file at
    // run time.
    getData(cadetRecsPtr,numRecs);

    // We can also use calloc() to allocate a zeroed block of memory
    // the parameters are different for calloc() first is the number
    // of blocks to allocate, the second is the size of the blocks.
    MyCadetInfo* cadetRecsPtr2 = calloc(numRecs, sizeof(MyCadetInfo));
    getData(cadetRecsPtr2, numRecs);

    // We can resize and copy with a single call using realloc()
    // reallocate free's the memory allocated at the location given
    // after it copies to the new memory location
    MyCadetInfo* cadetRecsPtr3 = realloc(cadetRecsPtr,numRecs * 2 * sizeof(MyCadetInfo));

    // to get the 2nd array copied into this 3rd array we can do a memcpy()
    // this is tricky as the addresses are computed as offsets (C is smart enough) to
    // know the size of a MyCadetInfo struct for pointer math, however you must
    // specify some things as a number of types...as in the 3rd parameter to memcpy()
    memcpy(cadetRecsPtr3+numRecs,cadetRecsPtr2,numRecs * sizeof(MyCadetInfo));

    // If we don't free the memory pointed to by cadetRecsPtr2 we will have a memory leak
    // Why didn't we free cadetRecsPtr?
    free(cadetRecsPtr2);

    return 0;
}


/** ----------------------------------------------------------
 * getDataText() is used to read MyCadetInfo typed records
 * from a file
 * @param cadetRecords is a ptr to the array of cadet records
 * @param numRecs - the number of records to read from the file
 * @return number of records read
 * ----------------------------------------------------------
 */
int getData(MyCadetInfo* cadetRecsPtr, int numRecs) {

    // Open an input file for reading
    FILE *in = fopen("../Lessons/lsn15/lsn15Data.txt", "r");
    if (in == NULL) {
        printf("Error opening data file: %s.\n", strerror(errno));
        exit(1);
    }

    char firstName[30];
    char lastName[45];
    int numRead = 0;

    while (numRead < numRecs && !feof(in)) {
        fscanf(in, "%s %s %d %d %d", firstName, lastName, &cadetRecsPtr[numRead].age,
                &cadetRecsPtr[numRead].squad, &cadetRecsPtr[numRead].classYear);
        strcat(firstName, " ");
        strcpy(cadetRecsPtr[numRead].name, strcat(firstName, lastName));
        cadetRecsPtr[numRead].numGrades = 4;
        for (int i=0; i<cadetRecsPtr[numRead].numGrades; i++) {
            cadetRecsPtr[numRead].grades[i] = (int) random() % 101;
        }
        numRead++;
    }
    fclose(in);
    return numRead;
}