/** example02b.c
* ===========================================================
* Name: Troy Weingart, 4 Jan 18
* Section: n/a
* Project: Lesson 2 examples
* Purpose: Demonstrate basic c
* ===========================================================
*/
#include <stdio.h>
#include <string.h>

// function proto types for functions used
// in this file
void swapCallByValue(int x, int y);
void swapCallByReference(int* x, int* y );

int main() {
    int aNum = 50;
    int bNum = 57;

    // print out aNum and bNum
    printf("aNum = %d, bNum = %d\n", aNum,bNum);

    // call swap using call by Value
    swapCallByValue(aNum, bNum);
    printf("aNum = %d, bNum = %d\n", aNum,bNum);

    // call swap using call by reference
    swapCallByReference(&aNum, &bNum);
    printf("aNum = %d, bNum = %d\n", aNum,bNum);

    return 0;
}

/** ----------------------------------------------------------
 * swapCallByValue() swaps the values passed into the function
 * to the console
 * @param x first value
 * @param y second value
 * @return n/a
 */
void swapCallByValue(int x, int y) {
    int temp;

    temp = y;
    y = x;
    x = temp;
}

/** ----------------------------------------------------------
 * swapCallByReference() swaps the values passed into the function
 * to the console
 * @param x address of first value
 * @param y address of second value
 * @return n/a
 */
void swapCallByReference(int* x, int* y) {
    int temp;

    temp = *y;
    *y = *x;
    *x = temp;
}