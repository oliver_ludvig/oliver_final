/** example02a.c
* ===========================================================
* Name: Troy Weingart, 4 Jan 18
* Section: n/a
* Project: Lesson 2 examples
* Purpose: Demonstrate basic c
* ===========================================================
*/
#include <stdio.h>
#include <string.h>

// Define a struct for a student record at USAFA
// note - we do not have a variable (memory) to store
// cadet information yet. This definition it is done
// above main so that all functions can use a
// cadetInfo struct
struct cadetInfo {
    char name[50];
    int age;
    int squad;
    int classYear;
};

// Another more common technique for defining a
// struct is to "create" our own custom data type
// using a typedef statement, here we create a new
// type called MyCadetInfo, by convention new types
// start with a capital letter...again no variable
// or memory has been set aside for our structure
typedef struct {
    char name[50];
    int age;
    int squad;
    int classYear;
} MyCadetInfo;

// Prototypes for function to print a cadetInfo struct,
// remember everything in C must be declared before use
void printCadetInfo(struct cadetInfo cadetRecord);
void printCadetInfo2(MyCadetInfo cadetRecord);

int main() {

    // We will now declare and initialize a cadet struct
    struct cadetInfo aCadet = {"Da Cadet", 19, 17, 20};
    struct cadetInfo bCadet;

    // Print out the struct using function
    printCadetInfo(aCadet);
    printCadetInfo(bCadet);

    // We can also make assignments using the assignment operator
    bCadet = aCadet;
    printCadetInfo(bCadet);

    // We can also assign new values to a cadet struct
    bCadet.classYear = 2018;
    bCadet.squad = 1;
    bCadet.age = 21;
    // why is this wrong in C?
    // bCadet.name = "Da Firstee";
    strncpy(bCadet.name,"Da Firstee",20);
    printCadetInfo(bCadet);

    //Lets define a third cadet record using our typedef
    MyCadetInfo cCadet = {"Da Third Cadet", 21, 40, 23};

    // Wy can't we do the following?
//    printCadetInfo(cCadet);
//    aCadet = cCadet;

    printCadetInfo2(cCadet);
    return 0;
}

/** ----------------------------------------------------------
 * printCadetInfo() is used to print a cadetInfo struct
 * to the console
 * @param cadetRecord is the cadetInfo struct to be printed
 * @return n/a
 */
void printCadetInfo(struct cadetInfo cadetRecord) {
    printf("Cadet name = \t%s\n", cadetRecord.name);
    printf("Cadet age = \t%d\n", cadetRecord.age);
    printf("Cadet squad = \t%d\n", cadetRecord.squad);
    printf("Cadet year = \t%d\n\n", cadetRecord.classYear);
}

void printCadetInfo2(MyCadetInfo cadetRecord) {
    printf("Cadet name = \t%s\n", cadetRecord.name);
    printf("Cadet age = \t%d\n", cadetRecord.age);
    printf("Cadet squad = \t%d\n", cadetRecord.squad);
    printf("Cadet year = \t%d\n\n", cadetRecord.classYear);
}