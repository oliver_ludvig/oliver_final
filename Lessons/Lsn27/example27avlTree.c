//
// Created by Wayne.Brown on 3/6/2017.
//

#include <stdlib.h>
#include <stdio.h>
#include "example27avlTree.h"

#define FALSE  0
#define TRUE   1

//---------------------------------------------------------------------
// Helper functions
void          avlUpdateHeight(AvlTreeNode * tree);
AvlTreeNode * avlInsertRecursive(AvlTreeNode * tree, AvlTreeNode * newNode );
AvlTreeNode * avlDeleteRecursive(AvlTreeNode * parent, AvlTreeNode * node, ElementType key);
void          avlPrintRecursive(AvlTreeNode * tree);
void          avlPrintRecursive2(AvlTreeNode * tree);
AvlTreeNode * avlRebalance(AvlTreeNode * tree);
int           getTreeHeight(AvlTreeNode * tree);
int           getHeightDifference(AvlTreeNode * tree);

//---------------------------------------------------------------------
AvlTreeNode * avlCreateTree() {
    return NULL;
}

//---------------------------------------------------------------------
AvlTreeNode * avlDeleteTree(AvlTreeNode * tree) {
    if (tree != NULL) {
        avlDeleteTree(tree->leftTree);
        avlDeleteTree(tree->rightTree);
        free(tree);
    }
    return NULL;
}

//---------------------------------------------------------------------
AvlTreeNode * avlInsert(AvlTreeNode * tree, ElementType newValue) {
    // Create a new node
    AvlTreeNode * newNode = (AvlTreeNode *) malloc(sizeof(AvlTreeNode));
    if (newNode == NULL) {
        printf("Error in avlInsert. No more memory!");
        exit(1);
    }

    // Initialize the newNode's values
    newNode->data = newValue;
    newNode->leftTree = NULL;
    newNode->rightTree = NULL;
    newNode->height = 1;

    if (tree == NULL) { // the tree is empty
        return newNode; // this is now the root node
    } else {
        return avlInsertRecursive(tree, newNode);
    }
}

//---------------------------------------------------------------------
// This takes care of an empty tree or starts the recursive from the
// root node.
AvlTreeNode * avlDelete(AvlTreeNode * root, ElementType key) {
    AvlTreeNode * newRoot = NULL;
    if ( root == NULL) {
        return NULL;
    } else {
        // There is at least one node for the root.
        if ( root->data == key) {
            if ( root->leftTree == NULL &&  root->rightTree == NULL) {
                // Case 1: the root node has no children
                free( root);
                return NULL;
            } else if ( root->leftTree != NULL &&  root->rightTree != NULL) {
                // Case 2: the node has two children
                AvlTreeNode * node =  root->leftTree;
                while (node->rightTree != NULL) {
                    node = node->rightTree;
                }
                // swap left node's data with this node's data
                ElementType temp = node->data;
                node->data =  root->data;
                root->data = temp;
                newRoot = avlDeleteRecursive( root, root->leftTree, key);
            } else if ( root->leftTree != NULL) {
                // Case 3: the node has one child on the left
                newRoot =  root->leftTree;
                free( root);
            } else {
                // Case 4: the node has one child on the right
                newRoot =  root->rightTree;
                free( root);
            }
        } else {
            if (key < root->data) {
                newRoot = avlDeleteRecursive( root, root->leftTree, key);
            } else {
                newRoot = avlDeleteRecursive( root, root->rightTree, key);
            }
        }
    }
    avlUpdateHeight(newRoot);

    if (getHeightDifference(newRoot) > 1) {
        // Re-balance this sub-tree
        printf("Rebalancing at node %d\n", newRoot->data);
        avlPrint(newRoot);
        newRoot = avlRebalance(newRoot);
    }
    printf("avlDelete return root as %d\n", newRoot->data);
    return newRoot;
}

int avlDeleteOneNode(AvlTreeNode * parent,
                     AvlTreeNode * node,
                     ElementType key) {
    if (node == NULL) return FALSE;

    if (node->data == key) {
        if (node->leftTree == NULL && node->rightTree == NULL) {
            // Case 1: the node has no children; just delete it
            if (parent->leftTree == node) {
                parent->leftTree = NULL;
            } else {
                parent->rightTree = NULL;
            }
            free(node);
            return TRUE;
        } else if (node->leftTree != NULL && node->rightTree != NULL) {
            // Case 2: the node has two children; mode data to bottom of tree
            AvlTreeNode *bottom = node->leftTree;
            while (bottom->rightTree != NULL) {
                bottom = bottom->rightTree;
            }
            // swap node's data to the bottom of the tree
            ElementType temp = node->data;
            node->data = bottom->data;
            bottom->data = temp;
            return FALSE;
        } else if (node->leftTree != NULL) {
            // Case 3: the node has one child on the left
            if (parent->leftTree == node) {
                parent->leftTree = node->leftTree;
            } else {
                parent->rightTree = node->leftTree;
            }
            free(node);
            return TRUE;
        } else {
            // Case 4: the node has one child on the right
            if (parent->leftTree == node) {
                parent->leftTree = node->rightTree;
            } else {
                parent->rightTree = node->rightTree;
            }
            free(node);
            return TRUE;
        }
    } else {
        return FALSE;
    }
}

AvlTreeNode * avlDeleteRecursive(AvlTreeNode * parent,
                                 AvlTreeNode * node,
                                 ElementType key) {
    if (node == NULL) {
        return parent;
    } else if (node->data == key) {
        if (avlDeleteOneNode(parent, node, key)) {
            return parent;
        } else {
            parent = avlDeleteRecursive(node, node->leftTree, key);
        }
    } else if (key < node->data) {
        parent = avlDeleteRecursive(node, node->leftTree, key);
    } else {
        parent = avlDeleteRecursive(node, node->rightTree, key);
    }

    avlUpdateHeight(parent);

    if (getHeightDifference(parent) > 1) {
        // Re-balance this sub-tree
        printf("Rebalancing at node %d\n", parent->data);
        avlPrint(parent);
        parent = avlRebalance(parent);
    }
    printf("avlDeleteRecursive return root as %d\n", parent->data);
    return parent;
}

//---------------------------------------------------------------------
AvlTreeNode * avlFind  (AvlTreeNode * tree, ElementType key) {
    if (tree == NULL) {
        return NULL;
    } else if (tree->data == key) {
        return tree;
    } else if (tree->data > key) {
        return avlFind(tree->leftTree, key);
    } else {
        return avlFind(tree->rightTree, key);
    }
}

//---------------------------------------------------------------------
void avlPrint(AvlTreeNode * tree) {
    printf("avlTree values in a in-order traversal:\n");
    avlPrintRecursive(tree);
    avlPrintRecursive2(tree);
    printf("\n");
}

void avlPrintRecursive(AvlTreeNode * tree) {
    int diff;
    if (tree != NULL) {
        avlPrintRecursive(tree->leftTree);
        if (tree->leftTree == NULL) {
            printf("LEFT: NULL       ");
        } else {
            AvlTreeNode * left = tree->leftTree;
            diff = getHeightDifference(left);
            printf("LEFT: %2d (%2d,%2d) ", left->data, left->height, diff);
        }
        diff = getHeightDifference(tree);
        printf("Node: %2d (%2d,%2d) ", tree->data, tree->height, diff);
        if (tree->rightTree == NULL) {
            printf("RIGHT: NULL\n");
        } else {
            AvlTreeNode * right = tree->rightTree;
            diff = getHeightDifference(right);
            printf("RIGHT: %2d (%2d,%2d)\n", right->data, right->height, diff);
        }
        avlPrintRecursive(tree->rightTree);
    }
}

//---------------------------------------------------------------------
// Define a queue to print the tree one level at a time.
#define MAX_NODES  128
AvlTreeNode * queue[MAX_NODES];
int           queueFirst;
int           queueLast;
// Assuming a maximum tree height of 5, here is the spacing for each line.
int initialSpace[6] = {60, 28, 12, 4, 0, 0};
int innerSpace[6] = {0, 56, 24, 8, 0, 0};

void avlPrintRecursive2(AvlTreeNode * tree) {
    int level = 0;
    int nodesPerLevel = 1;
    int nodesThisLevel = 0;
    int lineDivisor = 2;
    int numSpaces = 80 / lineDivisor;
    int nodesPrintedThisLevel = 0;
    queueFirst = 0;
    queueLast = 1;
    queue[queueFirst] = tree;
    int divisor = 2;
    while (queueFirst < queueLast && queueFirst < MAX_NODES) {
        // Get the node on the queue, print it
        AvlTreeNode * node = queue[queueFirst++];

        // print the spaces between nodes
        if (nodesThisLevel == 0) {
            numSpaces = initialSpace[level];
        } else {
            numSpaces = innerSpace[level];
        }
        for (int s=0; s<numSpaces; s++) {
            printf(" ");
        }
        // print the node
        if (node != NULL) {
            nodesPrintedThisLevel++;
            printf("%2d (%1d,%1d)", node->data, node->height, getHeightDifference(node));
            // Put all this node's children on the stack. Put NULL for missing children.
            queue[queueLast++] = node->leftTree;
            queue[queueLast++] = node->rightTree;
        } else {
            printf("--------");
            // Put two place holders on the stack for these missing children.
            queue[queueLast++] = NULL;
            queue[queueLast++] = NULL;
        }

        nodesThisLevel++;
        if (nodesThisLevel >= nodesPerLevel) {
            level++;
            printf("\n");
            if (nodesPrintedThisLevel == 0 || level > 5) break;
            nodesPerLevel *= 2;
            nodesThisLevel = 0;
            divisor += 1;
            numSpaces = (80 - (8 * nodesPerLevel)) / divisor ;
            nodesPrintedThisLevel = 0;
        }
    }
}
//---------------------------------------------------------------------
int avlVerifyHeightBalanced(AvlTreeNode * tree) {
    if (tree == NULL) {
        return 0;
    } else {
        int leftHeight = avlVerifyHeightBalanced(tree->leftTree);
        int rightHeight = avlVerifyHeightBalanced(tree->rightTree);

        if (leftHeight > rightHeight) {
            tree->height = leftHeight + 1;
        } else {
            tree->height = rightHeight + 1;
        }
        int difference = abs(leftHeight - rightHeight);
        if (difference > 1) {
            printf("Tree is not balanced. For node %d, height difference = %d\n", tree->data, difference);
        }
        return tree->height;
    }
}

//---------------------------------------------------------------------
AvlTreeNode * avlInsertRecursive(AvlTreeNode * tree, AvlTreeNode * newNode ) {
    AvlTreeNode * newRoot;
    if (newNode->data < tree->data) {
        if (tree->leftTree == NULL) {
            tree->leftTree = newNode;
        } else {
            tree->leftTree = avlInsertRecursive(tree->leftTree, newNode);
        }
    } else { // newNode->data > tree->data
        if (tree->rightTree == NULL) {
            tree->rightTree = newNode;
        } else {
            tree->rightTree = avlInsertRecursive(tree->rightTree, newNode);
        }
    }

    avlUpdateHeight(tree);

    if (getHeightDifference(tree) > 1) {
        // Re-balance this sub-tree
        printf("Rebalancing at node %d\n", tree->data);
        avlPrint(tree);
        tree = avlRebalance(tree);
    }
    return tree;
}

//---------------------------------------------------------------------
int getHeightDifference(AvlTreeNode * tree) {
    if (tree == NULL) {
        return 0;
    } else {
        int leftHeight = getTreeHeight(tree->leftTree);
        int rightHeight = getTreeHeight(tree->rightTree);
        return abs(leftHeight - rightHeight);
    }
}

//---------------------------------------------------------------------
AvlTreeNode * avlRebalance(AvlTreeNode * tree) {
    // This function is only called if heightDifference(tree) == 2
    // Execute the correct rotation of the 4 cases possible.
    AvlTreeNode * a = tree;
    AvlTreeNode * b;
    AvlTreeNode * c;
    AvlTreeNode * d;
    AvlTreeNode * newRoot;
    if (getHeightDifference(tree->leftTree) == 1) {
        b = a->leftTree;
        c = b->leftTree;
        d = b->rightTree;
        if (getTreeHeight(c) > getTreeHeight(d)) {
            printf("Re-balancing LEFT_LEFT node is %d\n", a->data);
            b->rightTree = a;
            a->leftTree = d;
            avlUpdateHeight(a);
            avlUpdateHeight(b);
            newRoot = b;
        } else {
            printf("Re-balancing LEFT_RIGHT node is %d\n", tree->data);
            AvlTreeNode * dLeft = d->leftTree;
            AvlTreeNode * dRight = d->rightTree;
            d->leftTree = b;
            d->rightTree = a;
            b->rightTree = dLeft;
            a->leftTree = dRight;
            avlUpdateHeight(a);
            avlUpdateHeight(b);
            avlUpdateHeight(d);
            newRoot = d;
        }
    } else { // tree->rightTree->heightDifference == 1
        AvlTreeNode * child = tree->rightTree;
        b = a->rightTree;
        c = b->rightTree;
        d = b->leftTree;
        if (getTreeHeight(c) > getTreeHeight(d)) {
            printf("Re-balancing RIGHT_RIGHT node is %d\n", tree->data);
            b->leftTree = a;
            a->rightTree = d;
            avlUpdateHeight(a);
            avlUpdateHeight(b);
            newRoot = b;
        } else {
            printf("Re-balancing RIGHT_LEFT node is %d\n", tree->data);
            AvlTreeNode * dLeft = d->leftTree;
            AvlTreeNode * dRight = d->rightTree;
            d->leftTree = a;
            d->rightTree = b;
            b->leftTree = dRight;
            a->rightTree = dLeft;
            avlUpdateHeight(a);
            avlUpdateHeight(b);
            avlUpdateHeight(d);
            newRoot = d;
        }
    }

    return newRoot;  // the new root of the sub-tree
}

//---------------------------------------------------------------------
int getTreeHeight(AvlTreeNode * tree) {
    if (tree == NULL) {
        return 0;
    } else {
        return tree->height;
    }
}

//---------------------------------------------------------------------
void avlUpdateHeight(AvlTreeNode * tree) {
    int leftHeight = getTreeHeight(tree->leftTree);
    int rightHeight = getTreeHeight(tree->rightTree);
    if (leftHeight > rightHeight) {
        tree->height = leftHeight + 1;
    } else {
        tree->height = rightHeight + 1;
    }
}



