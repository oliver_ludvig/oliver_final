//
// Created by Wayne.Brown on 3/6/2017.
//

#include <stdio.h>
#include <stdlib.h>
#include "example27avlTree.h"

int main() {
    AvlTreeNode *tree = NULL;

    tree = avlCreateTree();

    // Insert values into the tree
    for (int j=0; j<=100; j+=10) {
        printf("Inserting %d\n", j);
        tree = avlInsert(tree, j);
        avlPrint(tree);
        avlVerifyHeightBalanced(tree);
    }

    // Test deletions
    int valuesToDelete[] = {10, 30, 80, 0, 2, 70};
    int numToDelete = sizeof(valuesToDelete) / sizeof(int);
    for (int j=0; j<numToDelete; j++) {
        printf("deleting %d\n", valuesToDelete[j]);
        avlDelete(tree, valuesToDelete[j]);
        avlPrint(tree);
        avlVerifyHeightBalanced(tree);
    }
}
