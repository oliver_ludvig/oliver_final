/** example06a.c
 * ===========================================================
 * Name: Troy Weingart
 * Section: n/a
 * Project: Lesson 6 Error Handling in C (brief example)
 * ===========================================================
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


double sqrtNonNeg(double val);

int main() {
    double num = 0;
    double retVal;

    // Get value from user
    printf("Enter a number: ");
    scanf("%lf", &num);

    // call function and capture return value
    retVal = sqrtNonNeg(num);
    // check for error and exit with exit()
    if (retVal == -1) {
        printf("error: sqrtNonNeg() requires input > 0\n");
        exit(1);
    }
    printf("sqrt = %lf",retVal);

    return 0;
}

// calculates sqrt of positive number
double sqrtNonNeg(double val) {
    if (val < 0) {
        return -1;
    }
    return sqrt(val);
}